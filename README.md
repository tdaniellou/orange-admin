Orange Angular JS
=============

##To install all packages required to run the project
- Install node.js
- Set http and https-proxy for nodejs and bower:
	- npm config set http-proxy http://url:port/
	- npm config set https-proxy http://url:port/

npm install
bower install

##To start the Grunt server (development)
grunt serve

##To build the production
grunt build

##To launch unit tests
grunt test

##To launch e2e tests
export PATH=$PATH:$JAVA_HOME/bin
grunt protractor:e2e

##Version history
 - _0.0.1_  First version
 - _0.1.0_  End of sprint 1: Catalog List/create/edit/delete, Application & Universe List/delete
 - _0.2.0_  End of sprint 2: Application & Universe Create/edit, multiSelect directive, publish/rollback, unit/e2e tests, new theme
 - _0.3.0_  End of sprint 3: Global summary, summary diff, default and "orange" theme, new files organization, cleanups

##Known bugs
 - IE8: Input with placeholders and "required" attribute will mark them as "$dirty",
 		Application & Universe edit forms do not display the ID and the name,
 		Summaries are not always correct.
 - "Recharger la configuration de [...]": No changes on the adminTool, cache problem ?

##TODO
 - Fix multiple call of "buildMap" when universes and applications use CategoryService.getNames([categoriesIds]).
 	(Check the new "buildMap" & "getMap" methods in category.js)
