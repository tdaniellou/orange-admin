'use strict';

describe('Catalogs tests', function () {
	var ptor = protractor.getInstance();
	var baseUrl = ptor.params.url,
		catalogListUrl = ptor.params.url + '/#/catalogs',
		catalogCreateUrl = ptor.params.url + '/#/catalogs/new',
		catalogViewUrl = ptor.params.url + '/#/catalogs/view/' + ptor.params.catalog.validId,
		catalogEditUrl = ptor.params.url + '/#/catalogs/edit/' + ptor.params.catalog.validId;

	beforeEach(function () {
		ptor.manage().deleteAllCookies();

		browser.get(baseUrl + '/');

		element(by.model('user.name')).sendKeys(ptor.params.login.user);
		element(by.model('user.password')).sendKeys(ptor.params.login.password);
		element(by.id('submit')).click();
		browser.get(baseUrl + '/#/catalogs');
	});

	it('should land on "/catalogs"', function () {
		expect(browser.getCurrentUrl()).toEqual(catalogListUrl);
	});

	it('the edit bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('editBtn')).isEnabled()).toEqual(false);
	});

	it('the delete bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('deleteBtn')).isEnabled()).toEqual(false);
	});

	it('the checkAll input should be unchecked', function () {
		var checkAll = element(by.model('checkAll'));

		expect(checkAll.isSelected()).toBe(false);
	});

	it('should create a catalog only if every required fields are filled', function () {
		var send = element(by.id('submit'));

		element(by.id('createBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(catalogCreateUrl);

		expect(element(by.id('submit')).isEnabled()).toBe(false);

		element(by.model('catalog.id')).sendKeys(ptor.params.catalog.validId);
		expect(send.isEnabled()).toBe(false);

		element(by.model('catalog.id')).clear();

		element(by.model('catalog.type')).sendKeys('PASS');
		expect(send.isEnabled()).toBe(false);

		element(by.model('catalog.id')).sendKeys(ptor.params.catalog.validId);
		expect(send.isEnabled()).toBe(true);

		send.click();

		expect(browser.getCurrentUrl()).toEqual(catalogListUrl);
	});

	it('should filter the catalog list as user types into the search box', function () {
		var catalogList = element.all(by.repeater('row in rows'));
		var query = element(by.model('query.id'));

		query.sendKeys(ptor.params.catalog.validId);
		expect(catalogList.count()).toBe(1);
	});

	it('should view a catalog', function () {
		element(by.id(ptor.params.catalog.validId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(catalogViewUrl);

		var name = element(by.binding('catalogView.id'));
		expect(name.getText()).toBe(ptor.params.catalog.validId);
	});

	it('should edit a catalog', function () {
		element(by.id(ptor.params.catalog.validId)).click();
		element(by.id('editBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(catalogEditUrl);

		expect(element(by.model('catalog.id')).isEnabled()).toBe(false);

		element(by.model('catalog.type')).sendKeys('TRANSACTIONAL');

		element(by.id('submit')).click();

		expect(browser.getCurrentUrl()).toEqual(catalogListUrl);
	});

	it('should view the catalog\'s new values', function () {
		element(by.id(ptor.params.catalog.validId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(catalogViewUrl);

		var name = element(by.binding('catalogView.type'));
		expect(name.getText()).toBe('TRANSACTIONAL');
	});

	it('should delete a catalog', function () {
		element(by.id(ptor.params.catalog.validId)).click();
		element(by.id('deleteBtn')).click();

		element(by.buttonText('OK')).click();

		expect(element(by.id(ptor.params.catalog.validId)).isPresent()).toBe(false);

		expect(browser.getCurrentUrl()).toEqual(catalogListUrl);
	});
});