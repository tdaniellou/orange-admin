'use strict';

describe('Authentication tests', function () {
	var username = 'test';
	var password = '123';
	var ptor = protractor.getInstance();
	var baseUrl = ptor.params.url;

	beforeEach(function () {
		ptor.manage().deleteAllCookies();
		browser.get(baseUrl + '/');
	});

	it('Home page: login form should be present', function () {
		expect(element(by.name('loginForm')).isPresent()).toBe(true);
	});

	it('Home page: submit button should be disabled if username is filled but not the password', function () {

		element(by.model('user.name')).sendKeys(username);
		expect(element(by.id('submit')).isEnabled()).toEqual(false);
	});

	it('Home page: submit button should be disabled if password is filled but not the username', function () {

		element(by.model('user.password')).sendKeys(password);
		expect(element(by.id('submit')).isEnabled()).toEqual(false);
	});

	it('Home page: submit button should be disabled if username is filled but not the password', function () {

		element(by.model('user.name')).sendKeys(username);
		element(by.model('user.password')).sendKeys(password);
		expect(element(by.id('submit')).isEnabled()).toEqual(true);
	});

	it('Home page: should redirect to "/home" if the user is not logged in', function () {
		browser.get(baseUrl + '/#/applications');
		expect(browser.getLocationAbsUrl()).toMatch('/home');

		browser.get(baseUrl + '/#/randomurl');
		expect(browser.getLocationAbsUrl()).toMatch('/home');
	});

	it('Home page: should stay on "/home" if username and password are wrong', function () {

		element(by.model('user.name')).sendKeys(username);
		element(by.model('user.password')).sendKeys(password);
		element(by.id('submit')).click();

		expect(browser.getLocationAbsUrl()).toMatch('/home');
	});

	it('Home page: should land on "/summary" if username and password are correct', function () {

		element(by.model('user.name')).sendKeys(ptor.params.login.user);
		element(by.model('user.password')).sendKeys(ptor.params.login.password);
		element(by.id('submit')).click();

		expect(browser.getLocationAbsUrl()).toMatch('/summary');
	});
});