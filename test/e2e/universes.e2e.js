'use strict';

describe('Universes tests', function () {
	var ptor = protractor.getInstance(),
		baseUrl = ptor.params.url,
		validUniverseId = ptor.params.universe.validVodApp + '-' + ptor.params.universe.validId,
		universeListUrl = ptor.params.url + '/#/universes',
		universeCreateUrl = ptor.params.url + '/#/universes/new',
		universeEditUrl = ptor.params.url + '/#/universes/edit/' + validUniverseId,
		universeViewUrl = ptor.params.url + '/#/universes/view/' + validUniverseId,
		editUniverseName = ptor.params.universe.validName + '_2';

	beforeEach(function () {
		ptor.manage().deleteAllCookies();

		browser.get(baseUrl + '/');

		element(by.model('user.name')).sendKeys(ptor.params.login.user);
		element(by.model('user.password')).sendKeys(ptor.params.login.password);
		element(by.id('submit')).click();
		browser.get(baseUrl + '/#/universes');
	});

	it('should land on "/universes"', function () {
		expect(browser.getCurrentUrl()).toEqual(universeListUrl);
	});

	it('should filter the universe list as user types into the search box', function () {
		var query = element(by.model('query.id'));

		query.sendKeys('zzzzzzzzzzzzz');
		expect(element.all(by.css('.itemCheckbox')).count()).toBe(0);
	});

	it('the edit bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('editBtn')).isEnabled()).toEqual(false);
	});

	it('the delete bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('deleteBtn')).isEnabled()).toEqual(false);
	});

	it('should create a universe only if every required fields are filled', function () {
		var send = element(by.id('submit'));

		element(by.id('createBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(universeCreateUrl);

		expect(send.isEnabled()).toBe(false);

		element(by.model('universe.id')).sendKeys(ptor.params.universe.validId);
		expect(send.isEnabled()).toBe(false);

		element(by.model('universe.id')).clear();

		element(by.model('universe.vodApplicationId')).sendKeys(ptor.params.universe.validVodApp);
		expect(send.isEnabled()).toBe(false);

		element(by.model('universe.name')).sendKeys(ptor.params.universe.validName);
		expect(send.isEnabled()).toBe(false);

		element(by.model('universe.id')).sendKeys(ptor.params.universe.validId);
		expect(send.isEnabled()).toBe(true);

		send.click();

		expect(browser.getCurrentUrl()).toEqual(universeListUrl);

		expect(element(by.id(validUniverseId)).isPresent()).toBe(true);
	});

	it('should filter the universe list as user types into the search box', function () {
		var query = element(by.model('query.id'));

		query.sendKeys(ptor.params.universe.validId);

		expect(element.all(by.css('.itemCheckbox')).count()).toBe(1);
	});

	it('should view a universe', function () {
		element(by.id(validUniverseId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(universeViewUrl);

		var name = element(by.binding('universeView.name'));
		expect(name.getText()).toBe(ptor.params.universe.validName);
	});

	it('should edit a universe', function () {
		var send = element(by.id('submit'));

		element(by.id(validUniverseId)).click();
		element(by.id('editBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(universeEditUrl);

		expect(element(by.model('universe.id')).isEnabled()).toBe(false);
		expect(element(by.model('universe.vodApplicationId')).isEnabled()).toBe(false);

		element(by.model('universe.name')).clear();
		expect(send.isEnabled()).toBe(false);

		element(by.model('universe.name')).sendKeys(editUniverseName);
		expect(send.isEnabled()).toBe(true);

		element(by.id('submit')).click();
		expect(browser.getCurrentUrl()).toEqual(universeListUrl);
	});


	it('should view the universe\'s new values', function () {
		element(by.id(validUniverseId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(universeViewUrl);

		var name = element(by.binding('universeView.name'));
		expect(name.getText()).toBe(editUniverseName);
	});

	it('should delete a universe', function () {
		element(by.id(validUniverseId)).click();
		element(by.id('deleteBtn')).click();

		element(by.buttonText('OK')).click();

		expect(element(by.id(validUniverseId)).isPresent()).toBe(false);
	});
});