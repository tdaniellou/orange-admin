'use strict';

describe('Applications tests', function () {
	var ptor = protractor.getInstance();
	var baseUrl = ptor.params.url,
		applicationListUrl = ptor.params.url + '/#/applications',
		applicationCreateUrl = ptor.params.url + '/#/applications/new',
		applicationEditUrl = ptor.params.url + '/#/applications/edit/' + ptor.params.application.validId,
		applicationViewUrl = ptor.params.url + '/#/applications/view/' + ptor.params.application.validId;

	beforeEach(function () {
		ptor.manage().deleteAllCookies();

		browser.get(baseUrl + '/');

		element(by.model('user.name')).sendKeys(ptor.params.login.user);
		element(by.model('user.password')).sendKeys(ptor.params.login.password);
		element(by.id('submit')).click();
		browser.get(baseUrl + '/#/applications');
	});

	it('should land on "/applications"', function () {
		expect(browser.getCurrentUrl()).toEqual(applicationListUrl);
	});

	it('should filter the application list as user types into the search box', function () {
		var applicationList = element.all(by.repeater('row in rows'));
		var query = element(by.model('query.id'));

		query.sendKeys('zzzzzzzzzzzzz');
		expect(applicationList.count()).toBe(0);
	});

	it('the edit bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('editBtn')).isEnabled()).toEqual(false);
	});

	it('the delete bouton should be disabled if nothing is selected', function () {
		expect(element(by.id('deleteBtn')).isEnabled()).toEqual(false);
	});

	it('the checkAll input should be unchecked', function () {
		var checkAll = element(by.model('checkAll'));

		expect(checkAll.isSelected()).toBe(false);
	});

	it('should create an application only if every required fields are filled', function () {
		var send = element(by.id('submit'));

		element(by.id('createBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(applicationCreateUrl);

		expect(element(by.id('submit')).isEnabled()).toBe(false);

		element(by.model('application.id')).sendKeys(ptor.params.application.validId);
		expect(send.isEnabled()).toBe(true);

		element(by.model('application.id')).clear();
		expect(send.isEnabled()).toBe(false);

		element(by.model('qualities')).sendKeys(ptor.params.application.validQuality);
		expect(send.isEnabled()).toBe(false);

		element(by.model('application.id')).sendKeys(ptor.params.application.validId);
		expect(send.isEnabled()).toBe(true);

		send.click();

		expect(browser.getCurrentUrl()).toEqual(applicationListUrl);

		expect(element(by.id(ptor.params.application.validId)).isPresent()).toBe(true);
	});

	it('should filter the application list as user types into the search box', function () {
		var applicationList = element.all(by.repeater('row in rows'));
		var query = element(by.model('query.id'));

		query.sendKeys(ptor.params.application.validId);
		expect(applicationList.count()).toBe(1);
	});

	it('should view an application', function () {
		element(by.id(ptor.params.application.validId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(applicationViewUrl);

		var name = element(by.binding('applicationView.id'));
		expect(name.getText()).toBe(ptor.params.application.validId);
	});

	it('should edit an application', function () {
		element(by.id(ptor.params.application.validId)).click();
		element(by.id('editBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(applicationEditUrl);

		expect(element(by.model('application.id')).isEnabled()).toBe(false);

		// TODO: Change values

		element(by.id('submit')).click();

		expect(browser.getCurrentUrl()).toEqual(applicationListUrl);
	});

	it('should view a application\'s new values', function () {
		element(by.id(ptor.params.application.validId)).click();
		element(by.id('viewBtn')).click();

		expect(browser.getCurrentUrl()).toEqual(applicationViewUrl);

		// TODO: Check the modified value
		var name = element(by.binding('applicationView.id'));
		expect(name.getText()).toBe(ptor.params.application.validId);
	});

	it('should delete an application', function () {
		element(by.id(ptor.params.application.validId)).click();
		element(by.id('deleteBtn')).click();

		element(by.buttonText('OK')).click();

		expect(element(by.id(ptor.params.application.validId)).isPresent()).toBe(false);
	});
});