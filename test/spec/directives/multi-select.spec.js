'use strict';

describe('multiSelect', function () {
	var scope, isolatedScope, element, expanded, selected, stateChecked;

	// Build a tree for testing
	var subSubChild1 = {'id': 'SubSubChild1', 'parents': ['Parent', 'Child1', 'SubChild1'], 'children': []},
		subChild1 = {'id': 'SubChild1', 'parents': ['Parent', 'Child1'], 'children': [subSubChild1]},
		subChild2 = {'id': 'SubChild2', 'parents': ['Parent', 'Child1'], 'children': []},
		subChild3 = {'id': 'SubChild3', 'parents': ['Parent', 'Child2'], 'children': []},
		child1 = {'id': 'Child1', 'parents': ['Parent'], 'children': [subChild1, subChild2]},
		child2 = {'id': 'Child2', 'parents': ['Parent'], 'children': [subChild3]},
		child3 = {'id': 'Child3', 'parents': ['Parent'], 'children': []},
		parent = { 'id': 'Parent', 'children': [child1, child2, child3]};

	// Store the children
	var children = [
		child1,
		child2,
		child3
	];

	// Store the sub children
	var subChildren = [
		subChild1,
		subChild2,
		subChild3
	];

	// Load the directive and the view
	beforeEach(module('multiSelect', 'modules/multiSelect/multiSelect.tpl.html'));

	// Create the scope, store the tree and create the output
	beforeEach(inject(function ($rootScope) {
		scope = $rootScope.$new();
		scope.tree = {'Tree': parent};
		scope.output = {};
	}));

	describe('(No optional parameters)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('hasChildren should be true if the item has children', function () {
			expect(isolatedScope.hasChildren(parent)).toBeTruthy();
			expect(isolatedScope.hasChildren(child1)).toBeTruthy();
			expect(isolatedScope.hasChildren(child2)).toBeTruthy();
			expect(isolatedScope.hasChildren(subChild1)).toBeTruthy();
		});

		it('hasChildren should be false if the item doesn\'t have children', function () {
			expect(isolatedScope.hasChildren(child3)).toBeFalsy();
			expect(isolatedScope.hasChildren(subChild2)).toBeFalsy();
		});

		it('hasParents should be true if the item has parents', function () {
			for (var i in children) {
				expect(isolatedScope.hasParents(children[i])).toBeTruthy();
			}

			for (var j in subChildren) {
				expect(isolatedScope.hasParents(subChildren[j])).toBeTruthy();
			}
		});

		it('hasCheckedChildren should be true if an item has children that are selected', function () {
			isolatedScope.selectItem(subChild1);

			expect(isolatedScope.hasCheckedChildren(child1)).toBeTruthy();
		});

		it('hasCheckedChildren should be false if an item has no children selected', function () {
			expect(isolatedScope.isSelected(subChild1)).toBeFalsy();
			expect(isolatedScope.hasCheckedChildren(child1)).toBeFalsy();
		});

		it('hasCheckedChildren should be false if an item has no children', function () {
			expect(isolatedScope.hasCheckedChildren(child3)).toBeFalsy();
		});

		it('children and sub children should be collapsed at start', function () {
			for (var i in children) {
				expanded = isolatedScope.isExpanded(children[i]);
				expect(expanded).toBeFalsy();
			}

			for (var j in subChildren) {
				expanded = isolatedScope.isExpanded(subChildren[j]);
				expect(expanded).toBeFalsy();
			}
		});

		it('children ans sub hildren should NOT be selected at start', function () {
			for (var i in children) {
				selected = isolatedScope.isSelected(children[i]);
				expect(selected).toBeFalsy();
			}

			for (var j in subChildren) {
				selected = isolatedScope.isSelected(subChildren[j]);
				expect(selected).toBeFalsy();
			}
		});

		it('an item should have the unchecked icon at start', function () {
			for (var i in children) {
				stateChecked = isolatedScope.stateChecked(children[i]);
				expect(stateChecked['fa-square-o']).toBeTruthy();
			}

			for (var j in subChildren) {
				stateChecked = isolatedScope.stateChecked(subChildren[j]);
				expect(stateChecked['fa-square-o']).toBeTruthy();
			}
		});

		it('an item should NOT have the checked icon at start', function () {
			for (var i in children) {
				stateChecked = isolatedScope.stateChecked(children[i]);
				expect(stateChecked['fa-check-square-o']).toBeFalsy();
			}

			for (var j in subChildren) {
				stateChecked = isolatedScope.stateChecked(subChildren[j]);
				expect(stateChecked['fa-check-square-o']).toBeFalsy();
			}
		});

		it('an item should NOT have the middle checked icon at start', function () {
			for (var i in children) {
				stateChecked = isolatedScope.stateChecked(children[i]);
				expect(stateChecked['fa-minus-square-o']).toBeFalsy();
			}

			for (var j in subChildren) {
				stateChecked = isolatedScope.stateChecked(subChildren[j]);
				expect(stateChecked['fa-minus-square-o']).toBeFalsy();
			}
		});

		describe('an item should have the corresponding icon', function () {

			it('when selected', function () {
				isolatedScope.selectItem(child1);

				// Check if selected
				expect(isolatedScope.isSelected(child1)).toBeTruthy();

				// Check the icon
				stateChecked = isolatedScope.stateChecked(child1);
				expect(stateChecked['fa-square-o']).toBeFalsy();
				expect(stateChecked['fa-check-square-o']).toBeTruthy();
				expect(stateChecked['fa-minus-square-o']).toBeFalsy();
			});

			it('when not selected', function () {
				// Check if NOT selected
				expect(isolatedScope.isSelected(child1)).toBeFalsy();

				stateChecked = isolatedScope.stateChecked(child1);
				expect(stateChecked['fa-square-o']).toBeTruthy();
				expect(stateChecked['fa-check-square-o']).toBeFalsy();
				expect(stateChecked['fa-minus-square-o']).toBeFalsy();
			});

			it('when have children checked', function () {
				isolatedScope.selectItem(subChild1);

				// Check if NOT selected AND children is selected
				expect(isolatedScope.isSelected(child1)).toBeFalsy();
				expect(isolatedScope.isSelected(subChild1)).toBeTruthy();

				stateChecked = isolatedScope.stateChecked(child1);
				expect(stateChecked['fa-square-o']).toBeFalsy();
				expect(stateChecked['fa-check-square-o']).toBeFalsy();
				expect(stateChecked['fa-minus-square-o']).toBeTruthy();
			});
		});

		it('toggle on collapsed children should expand it and vice versa', function () {
			// Check if collapsed
			for (var i in children) {
				expect(isolatedScope.isExpanded(children[i])).toBeFalsy();
			}

			for (var j in children) {
				isolatedScope.toggle(children[j]);
				expect(isolatedScope.isExpanded(children[j])).toBeTruthy();
			}

			for (var k in children) {
				isolatedScope.toggle(children[k]);
				expect(isolatedScope.isExpanded(children[k])).toBeFalsy();
			}
		});

		it('collapseAll & expandAll should collapse/expand every children', function () {
			isolatedScope.collapseAll();

			for (var i in children) {
				expanded = isolatedScope.isExpanded(children[i]);
				expect(expanded).toBeFalsy();
			}

			isolatedScope.expandAll();

			for (var j in children) {
				expanded = isolatedScope.isExpanded(children[j]);
				expect(expanded).toBeTruthy();
			}
		});

		it('selectHelper should check/uncheck every children', function () {
			var selected;
			isolatedScope.selectHelper('all');

			for (var i in children) {
				selected = isolatedScope.isSelected(children[i]);
				expect(selected).toBeTruthy();
			}

			isolatedScope.selectHelper('none');

			for (var j in children) {
				selected = isolatedScope.isSelected(children[j]);
				expect(selected).toBeFalsy();
			}
		});

		it('should expand children and sub children when expanded', function () {
			isolatedScope.collapseAll();
			isolatedScope.expand(parent);

			for (var i in children) {
				expanded = isolatedScope.isExpanded(children[i]);
				expect(expanded).toBeTruthy();
			}

			for (var j in subChildren) {
				expanded = isolatedScope.isExpanded(subChildren[j]);
				expect(expanded).toBeTruthy();
			}
		});

		it('should expand children and sub children when selected', function () {
			isolatedScope.collapseAll();
			isolatedScope.selectItem(parent);

			for (var i in children) {
				expanded = isolatedScope.isExpanded(children[i]);
				expect(expanded).toBeTruthy();
			}

			for (var j in subChildren) {
				expanded = isolatedScope.isExpanded(subChildren[j]);
				expect(expanded).toBeTruthy();
			}
		});

		it('should select children when selected', function () {
			isolatedScope.selectItem(parent);

			for (var i in children) {
				expanded = isolatedScope.isExpanded(children[i]);
				expect(expanded).toBeTruthy();
			}

			for (var j in subChildren) {
				expanded = isolatedScope.isExpanded(subChildren[j]);
				expect(expanded).toBeTruthy();
			}
		});

		it('Items in the output object should be selected in the isolatedScope depending of their "selected" value', function () {
			scope.output = {'Child3': child3, 'SubChild1': subChild1, 'SubChild2': subChild2, 'SubChild3': subChild3};
			scope.output.Child3.selected = true;
			scope.output.SubChild2.selected = true;
			scope.output.SubChild1.selected = false;
			scope.output.SubChild3.selected = false;
			scope.$digest();

			for (var i in isolatedScope.output) {
				var outputObject = isolatedScope.output[i];
				if (outputObject.selected === true) {
					expect(isolatedScope.isSelected(outputObject)).toBeTruthy();
				} else {
					expect(isolatedScope.isSelected(outputObject)).toBeFalsy();
				}
			}
		});

		it('ReadOnly should be turned off', function () {
			expect(isolatedScope.readOnly).toBeFalsy();
		});
	});

	describe('(autoSelectChildren = false)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output" multi-select-children-auto="false"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('should NOT select children when selected', function () {
			isolatedScope.selectItem(child1);

			expect(isolatedScope.isSelected(subChild1)).toBeFalsy();
			expect(isolatedScope.isSelected(subChild2)).toBeFalsy();
		});
	});

	describe('(autoExpandChildren = false)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output" multi-select-auto-expand-children="false"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('item should NOT expand when selected', function () {
			isolatedScope.selectItem(child1);

			expect(isolatedScope.isExpanded(subChild1)).toBeFalsy();
		});
	});

	describe('(autoExpandSubChildren = false)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output" multi-select-auto-expand-sub-children="false"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('should NOT expand sub children when selected', function () {
			isolatedScope.selectItem(parent);

			expect(isolatedScope.isExpanded(subChild1)).toBeFalsy();
			expect(isolatedScope.isExpanded(subChild2)).toBeFalsy();
		});
	});

	describe('(autoDisableFamily = true)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output" multi-select-auto-disable-family="true"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('items should NOT be disabled at start', function () {
			for (var i in children) {
				expect(isolatedScope.isDisabled(children[i])).toBeFalsy();
			}
		});

		it('should disable family when selected', function () {
			isolatedScope.selectItem(subChild1);

			expect(isolatedScope.isDisabled(subSubChild1)).toBeTruthy();

			expect(isolatedScope.isDisabled(child1)).toBeTruthy();
		});

		it('should not be selected when disabled', function () {
			isolatedScope.selectItem(subChild1);

			expect(isolatedScope.isDisabled(subSubChild1)).toBeTruthy();
			expect(isolatedScope.isSelected(subSubChild1)).toBeFalsy();
		});

		it('should not be disabled when selected', function () {
			isolatedScope.selectItem(subSubChild1);

			// Try to disable the children
			isolatedScope.selectItem(subChild1);

			expect(isolatedScope.isDisabled(subSubChild1)).toBeFalsy();
		});

		it('should not be able to be selected when disabled', function () {
			isolatedScope.selectItem(subChild1);

			// Check if disabled
			isolatedScope.stateItem(subSubChild1);
			expect(isolatedScope.isDisabled(subSubChild1)).toBeTruthy();

			isolatedScope.selectItem(subSubChild1);
			expect(isolatedScope.isSelected(subSubChild1)).toBeFalsy();
		});

		it('should be disabled if one children or more are selected', function () {
			isolatedScope.selectItem(subChild1);
			isolatedScope.selectItem(subChild2);

			expect(isolatedScope.isDisabled(child1)).toBeTruthy();

			isolatedScope.selectItem(subChild1);
			expect(isolatedScope.isDisabled(child1)).toBeTruthy();

			isolatedScope.selectItem(subChild2);
			expect(isolatedScope.isDisabled(child1)).toBeFalsy();
		});

		it('should add the "disable" class if an item is disabled', function () {
			isolatedScope.selectItem(subChild1);

			// Check if item is disabled
			expect(isolatedScope.isDisabled(subSubChild1)).toBeTruthy();

			var stateItem = isolatedScope.stateItem(subSubChild1);
			expect(stateItem.disabled).toBeTruthy();
		});

		it('should add the "disable" class if an item has disabled children', function () {
			isolatedScope.selectItem(subSubChild1);

			// Check if item is disabled
			expect(isolatedScope.isDisabled(subChild1)).toBeTruthy();

			var stateItem = isolatedScope.stateItem(subChild1);
			expect(stateItem.disabled).toBeTruthy();
		});
	});

	describe('(readOnly = true)', function () {
		beforeEach(inject(function ($compile) {
			element = $compile('<div multi-select multi-select-input="tree" multi-select-output="output" multi-select-read-only="true"></div>')(scope);

			scope.$digest();

			isolatedScope = element.isolateScope();
		}));

		it('ReadOnly should be turned off', function () {
			expect(isolatedScope.readOnly).toBeTruthy();
		});

		it('should not select an item', function () {
			isolatedScope.selectItem(subSubChild1);

			expect(isolatedScope.isSelected(subSubChild1)).toBeFalsy();
		});

		it('should not display the checkbox', function () {
			for (var i in children) {
				stateChecked = isolatedScope.stateChecked(children[i]);
				expect(stateChecked.hide).toBeTruthy();
			}

			for (var j in subChildren) {
				stateChecked = isolatedScope.stateChecked(subChildren[j]);
				expect(stateChecked.hide).toBeTruthy();
			}
		});
	});
});