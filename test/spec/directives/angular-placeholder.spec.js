'use strict';


describe('test module angular-placeholder', function() {
	beforeEach(function() {
		module('ng.shims.placeholder');
	});

	describe('for an empty input field of type text using directive placeholder', function() {
		var emailField, scope;

		beforeEach(inject(function($rootScope, $compile) {
			scope = $rootScope.$new();
			emailField = angular.element('<input type="text" name="useremail" placeholder="E-Mail" ng-model="form.email" value="" />');
			$compile(emailField)(scope);
		}));

		it('should display the placeholder as input value', function() {
			expect(emailField.val()).toBe('E-Mail');
			expect(emailField.hasClass('placeholder')).toBe(true);
			expect(scope.form.email).toBe('');
		});

		describe('when input field gains focus', function() {
			beforeEach(function() {
				emailField.triggerHandler('focus');
			});

			afterEach(function() {
				emailField.triggerHandler('blur');
			});

			it('should hide the placeholder and remove class "empty"', function() {
				expect(emailField.val()).toBe('');
				expect(emailField.hasClass('placeholder')).toBe(false);
				expect(scope.form.email).toBe('');
			});

			it('should restore the placeholder and class "empty" when leaving field unchanged', function() {
				emailField.triggerHandler('blur');
				expect(emailField.val()).toBe('E-Mail');
				expect(emailField.hasClass('placeholder')).toBe(true);
				expect(scope.form.email).toBe('');
			});

			describe('when text is entered into the input field', function() {
				beforeEach(function() {
					emailField.val('me@example.com');
					emailField.triggerHandler('blur');
				});

				it('should remember the entered text in scope.form and remove class "empty"', function() {
					expect(emailField.val()).toBe('me@example.com');
					expect(emailField.hasClass('placeholder')).toBe(false);
					expect(scope.form.email).toBe('me@example.com');
				});

				it('should reset the field as "empty"', function() {
					emailField.triggerHandler('focus');
					emailField.val('');
					emailField.triggerHandler('blur');
					expect(emailField.val()).toBe('E-Mail');
					expect(emailField.hasClass('placeholder')).toBe(true);
					expect(scope.form.email).toBe('');
				});
			});
		});

		describe('when the value is set through the model controller', function() {
			beforeEach(function() {
				expect(emailField.val()).toBe('E-Mail');
				scope.form.email = 'john@doe.com';
				scope.$digest();
			});

			it('should display the value in the input field', function() {
				expect(emailField.val()).toBe('john@doe.com');
				expect(emailField.hasClass('placeholder')).toBe(false);
			});

			it('should replace an empty value in the input field by placeholder', function() {
				scope.form.email = '';
				scope.$digest();
				expect(emailField.val()).toBe('E-Mail');
				expect(emailField.hasClass('placeholder')).toBe(true);
			});

		});

	});

	describe('for input field of type password using directive placeholder', function() {
		var pwdField, pwdClone, scope;

		beforeEach(inject(function($rootScope, $compile) {
			scope = $rootScope.$new();
			pwdField = angular.element('<input type="password" name="userpwd" placeholder="Password" ng-model="form.passwd" value="" />');
			$compile(pwdField)(scope);
			pwdClone = angular.element(pwdField[0].previousElementSibling);
		}));

		it('should hide the password input, add class "empty", and show a separate placeholder text input', function() {
			expect(pwdField.val()).toBe('');
			expect(pwdField.attr('type')).toBe('password');
			expect(pwdField.hasClass('ng-hide')).toBe(true);
			expect(pwdField.hasClass('placeholder')).toBe(true);
			expect(pwdClone.val()).toBe('Password');
			expect(pwdClone.attr('type')).toBe('text');
			expect(pwdClone.hasClass('ng-hide')).toBe(false);
			expect(pwdClone.hasClass('placeholder')).toBe(true);
			expect(scope.form.passwd).toBe('');
		});

		describe('when password placeholder gains focus', function() {
			beforeEach(function() {
				pwdClone.triggerHandler('focus');
				pwdField.triggerHandler('focus');
			});

			afterEach(function() {
				pwdField.triggerHandler('blur');
			});

			it('should show the password input, remove class "empty", and hide the placeholder', function() {
				expect(pwdField.val()).toBe('');
				expect(pwdField.hasClass('ng-hide')).toBe(false);
				expect(pwdField.hasClass('placeholder')).toBe(false);
				expect(pwdClone.val()).toBe('Password');
				expect(pwdClone.hasClass('ng-hide')).toBe(true);
				expect(pwdClone.hasClass('placeholder')).toBe(true);
				expect(scope.form.passwd).toBe('');
			});
		});
	});
});
