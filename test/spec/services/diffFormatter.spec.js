'use strict';

describe('Service: DiffFormatter', function () {

	// load the modules
	beforeEach(module('ui.router'));
	beforeEach(module('angularApp.summary'));
	beforeEach(module('angularApp.utils'));

	var summary, DiffFormatter, clonedObject, diff;

	var clone = function (object) {
		if (typeof object === 'undefined') {
			return undefined;
		}
		return JSON.parse(JSON.stringify(object));
	};

	// Initialize the scope and the service
	beforeEach(inject(function (_DiffFormatter_) {
		DiffFormatter = _DiffFormatter_;
		summary = {
			'catalogs':[
				{
					'id':'Catalog1',
					'type':'Catalog1Type',
					'name':'Catalog 1'
				},
				{
					'id':'Catalog2',
					'type':'Catalog2Type',
					'name':''
				}
			],
			'applications':[
				{
					'id':'Application1',
					'filters':{
						'csa':[
							'1'
						],
						'deliveries':[
							'STREAMING'
						],
						'filteredQualities':[
							'3D'
						],
						'filteredCategories':[

						],
						'highlightItems':[

						]
					},
					'universes':[
						{
							'id':'Universe1',
							'vodApplicationId':'Application1',
							'uniqueId':'Application1-Universe1',
							'name':'Universe 1',
							'catalogId':'Catalog1',
							'categories':[
								'Cat1',
								'Cat2',
								'Cat3',
								'Cat4'
							]
						}
					]
				},
				{
					'id':'Application2',
					'filters':{
						'csa':[
							'1'
						],
						'deliveries':[
							'STREAMING'
						],
						'filteredQualities':[
							'3D'
						],
						'filteredCategories':[

						],
						'highlightItems':[

						]
					},
					'universes':[
						{
							'id':'Universe2',
							'vodApplicationId':'Application2',
							'uniqueId':'Application2-Universe2',
							'name':'Universe 2',
							'catalogId':'Catalog2',
							'categories':[
								'Cat5',
								'Cat6',
								'Cat7',
								'Cat8'
							]
						}
					]
				}
			]
		};
		clonedObject = clone(summary);
	}));

	it('Should have not have diff', function () {
		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).toBe(undefined);
	});

	it('for a modified application', function () {
		summary.applications[0].name = 'New Catalog name';

		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).not.toBe(undefined);
		expect(diff.diffs.applications.Application1).toBe('modified');
	});

	it('for a deleted application', function () {
		summary.applications.splice(0, 1);

		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).not.toBe(undefined);
		expect(diff.diffs.applications.Application1).toBe('deleted');
	});

	it('for a new application', function () {
		summary.applications.push({'id': '3'});

		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).not.toBe(undefined);
		expect(diff.diffs.applications['3']).toBe('added');
	});

	it('for a modified application', function () {
		summary.applications[0].filters.csa.push('2');

		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).not.toBe(undefined);
		expect(diff.diffs.applications.Application1).toBe('modified');
	});

	it('for an added universe)', function () {
		summary.applications[1].universes.push({'id': 'Universe3', 'vodApplicationId': 'Application2', 'uniqueId': 'Application2-Universe3'});

		diff = DiffFormatter.create(summary, clonedObject);
		expect(diff.delta).not.toBe(undefined);
		expect(diff.diffs.applications.Application2).toBe('modified');
		expect(diff.diffs.universes['Application2-Universe3']).toBe('added');
	});
});