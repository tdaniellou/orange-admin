'use strict';

describe('Service: AlertService', function () {

	// load the module
	beforeEach(module('angularApp.alert'));

	var AlertService, rootScope;

	// Initialize the service and a mock scope
	beforeEach(inject(function ($rootScope, _AlertService_) {
		AlertService = _AlertService_;
		rootScope = $rootScope.$new();

		AlertService.add('success', 'Success message');
		AlertService.add('info', 'Info message');
		AlertService.add('warning', 'Warning message');
		AlertService.add('danger', 'Danger message');
	}));

	it('should have four flash messages', function () {
		expect(rootScope.alerts.length).toBe(4);
	});

	it('should add one random flash messages', function () {
		var alert = {type: 'randomType', msg: 'randomMessage.', timeout: 100};

		AlertService.add(alert.type, alert.msg, alert.timeout);

		expect(rootScope.alerts).toContain(alert);
	});

	it('should close two flash messages', function () {
		AlertService.closeAlert(0);
		AlertService.closeAlert(1);

		expect(rootScope.alerts.length).toBe(2);
	});

	it('should clear all flash messages', function () {
		AlertService.clearAll();

		expect(rootScope.alerts.length).toBe(0);
	});
});
