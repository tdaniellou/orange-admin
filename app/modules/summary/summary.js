(function () {
	'use strict';

	/**
	 * Module
	 */
	angular.module('angularApp.summary', [])

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			// Global Summary
			.state('app.summary', {
				url: '/summary',
				templateUrl: 'modules/summary/summary.tpl.html',
				controller: 'SummaryCtrl',
				resolve: {
					workingSummary: ['SummaryService', function (SummaryService) {
						return SummaryService.getSummary();
					}]
				}
			});
	}])

	/**
	 * Summary service
	 */
	.factory('SummaryService', ['$q', 'ApplicationService', 'CatalogService', 'UniverseService',
		function ($q, ApplicationService, CatalogService, UniverseService) {
			var SummaryService = {};

			/**
			 * Get a state's summary
			 * @method getSummary
			 * @param {String} state
			 * @return promise
			 */
			SummaryService.getSummary = function (state) {
				var deferred = $q.defer();
				var summary = {};

				$q.all([ApplicationService.getAllViews(state), CatalogService.getAll(state)])
					.then(function (data) {
						var applications = data[0], catalogs = data[1];
						var universesPromises = [];

						summary.catalogs = catalogs;
						summary.applications = applications;

						// Get applications' universes
						angular.forEach(applications, function (applicationView) {
							var univViewPromise = UniverseService.getViewsByApplication(applicationView.id, state);
							universesPromises.push(univViewPromise);
						});

						// Sync
						$q.all(universesPromises).then(function (result) {
							for (var i in result) {
								summary.applications[i].universes = result[i]; // i should correspond
							}
							deferred.resolve(summary);
						});
					});

				return deferred.promise;
			};

			return SummaryService;
		}
	])

	/**
	 * Reload controller
	 */
	.controller('ReloadCtrl',
		['$scope', '$modalInstance', '$state', '$stateParams', '$q','$http', 'AlertService', 'State', 'ROLLBACKS_URLS',
		function ($scope, $modalInstance, $state, $stateParams, $q, $http, AlertService, State, ROLLBACKS_URLS) {
			$scope.message = 'Voulez vous vraiment recharger la configuration ' + State + ' ?';

			// reload
			$scope.reload = function () {
				var promises = [];
				var modules = ROLLBACKS_URLS;

				angular.forEach(modules, function (module) {
					var deferred = $q.defer();
					promises.push(deferred.promise);

					$http.get(module.url + '?state=' + State)
						.success(function () {
							deferred.resolve();
						})
						.error(function () {
							deferred.reject(module.name);
						});
				});

				$q.all(promises).then(function () {
					AlertService.add('success', 'Le rollback a réussi');
				}, function (error) {
					AlertService.add('danger', 'Le rollback a échoué (Cause : '+ error +').');
				});
			};

			$scope.confirm = function () {
				$scope.reload();
				$modalInstance.close();
				$state.go($state.current, $stateParams, {reload: true});
			};

			$scope.cancel = function () {
				$modalInstance.dismiss();
			};
		}
	])

	/**
	 * Summary controller
	 * Displays the WORK summary and a select to display the OLD or PROD summary
	 * then builds the diff between the two summaries.
	 */
	.controller('SummaryCtrl',
		['$scope', '$modal', 'SummaryService', 'Utils', 'workingSummary', 'DiffFormatter',
		function ($scope, $modal, SummaryService, Utils, workingSummary, DiffFormatter) {
			$scope.workingSummary = workingSummary;
			$scope.stateSummary = {};
			$scope.delta = null;
			$scope.diffs = {};

			$scope.displayLegend = false;

			// Models for the summary settings directive
			$scope.workPrefix = '';
			$scope.statePrefix = '';

			// Open/close model for the accordion
			$scope.opened = {
				'applications': {},
				'catalogs': {},
				'universes': {}
			};

			// Load the OLD or PROD summary
			$scope.loadState = function () {
				var state = $scope.state;
				$scope.displayLegend = false;

				if (state !== '') {
					SummaryService.getSummary(state).then(function (summary) {
						$scope.oneAtATime = true;
						$scope.stateSummary = summary;

						var diff = DiffFormatter.create($scope.workingSummary, $scope.stateSummary);

						$scope.delta = diff.delta;
						$scope.diffs = diff.diffs;
						$scope.displayLegend = true;
					});
				} else {
					$scope.oneAtATime = false;
					$scope.delta = null;
					$scope.diffs = {};
				}
			};

			$scope.reload = function (state) {
				$modal.open({
					controller: 'ReloadCtrl',
					templateUrl: 'modules/common/modal-confirm.tpl.html',
					resolve: {
						State: function () {
							return state;
						}
					}
				});
			};

			$scope.viewDiff = function () {
				$modal.open({
					templateUrl: 'modules/summary/modal-jsondiff.tpl.html',
					controller: 'JsonDiffCtrl',
					resolve: {
						workingSummary: function () {
							return Utils.cleanupAngularObject($scope.workingSummary);
						},
						delta: function () {
							return $scope.delta;
						}
					}
				});
			};
		}
	])

	/**
	 * Summary view directive
	 * Displays the diff in accordions
	 */
	.directive('summaryView', function () {
		return {
			restrict: 'AE',
			replace: true,
			scope: {
				data: '=summaryData',
				title: '@summaryTitle',
				opened: '=summaryOpened',
				diffs: '=summaryDiffs',
				oneAtATime: '=summaryCloseOthers',
				prefix: '=summaryOpenPrefix'
			},
			controller: ['$scope', function ($scope) {
				if (!$scope.diffs) { return; }

				var checkCatDiff = function () {
					return angular.isDefined($scope.diffs.catalogs);
				};

				var checkAppDiff = function () {
					return angular.isDefined($scope.diffs.applications);
				};

				var checkUnivDiff = function () {
					return angular.isDefined($scope.diffs.universes);
				};

				$scope.catDiffValue = function (id) {
					return {
						'catalog-diff-added': checkCatDiff() && $scope.diffs.catalogs[id] === 'added',
						'catalog-diff-modified': checkCatDiff() && $scope.diffs.catalogs[id] === 'modified',
						'catalog-diff-deleted': checkCatDiff() && $scope.diffs.catalogs[id] === 'deleted'
					};
				};

				$scope.appDiffValue = function (id) {
					return {
						'diff-added': checkAppDiff() && $scope.diffs.applications[id] === 'added',
						'diff-modified': checkAppDiff() && $scope.diffs.applications[id] === 'modified',
						'diff-deleted': checkAppDiff() && $scope.diffs.applications[id] === 'deleted'
					};
				};

				$scope.univDiffValue = function (id) {
					return {
						'diff-added': checkUnivDiff() && $scope.diffs.universes[id] === 'added',
						'diff-modified': checkUnivDiff() && $scope.diffs.universes[id] === 'modified',
						'diff-deleted': checkUnivDiff() && $scope.diffs.universes[id] === 'deleted'
					};
				};
			}],
			templateUrl: 'modules/summary/summary-view.tpl.html'
		};
	})

	/**
	 * SummarySettings directive
	 * Shows/hides the settings
	 */
	.directive('summarySettings',
		['ApplicationService', 'CatalogService', 'UniverseService', '$q',
		function (ApplicationService, CatalogService, UniverseService, $q) {
			return {
				replace: true,
				templateUrl: 'modules/summary/summary-settings.tpl.html',
				restrict: 'AE',
				link: function (scope, element, attrs) {
					scope.oneAtATime = false;

					// Open/close model for the setting toggle (closed at start)
					scope.settingsOpened = false;

					scope.toggleSettings = function () {
						scope.settingsOpened = !scope.settingsOpened;
					};

					// Open/Close all accordions (experimental)
					var toggleAccordions = function (action) {
						$q.all([ApplicationService.getAllIds(),UniverseService.getAllIds()])
							.then(function (data) {
								var appIds = data[0], univIds = data[1];
								scope.opened.catalogs = action;
								for (var i = 0; i < appIds.length; i++) {
									scope.opened.applications[appIds[i]] = action;
									scope.opened.applications['state_' + appIds[i]] = action;
								}
								for (var j = 0; j < univIds.length; j++) {
									scope.opened.universes[univIds[j]] = action;
									scope.opened.universes['state_' + univIds[j]] = action;
								}
							});
					};

					scope.openAll = function () {
						scope.oneAtATime = false;
						toggleAccordions(true);
					};

					scope.closeAll = function () {
						scope.oneAtATime = false;
						toggleAccordions(false);
					};
				}
			};
		}
	])

	/**
	 * Summary legend directive
	 * Displays the diff color legend and a message if there are diffs in catalogs or applications
	 */
	.directive('summaryLegend', function () {
		return {
			restrict: 'AE',
			templateUrl: 'modules/summary/summary-legend.tpl.html'
		};
	})

	/**
	 * JsonDiff controller
	 * Displays the formatted diff from a delta and the WORK summary
	 */
	.controller('JsonDiffCtrl',
		['$scope', '$modalInstance', 'workingSummary', 'delta', 'HtmlFormatter',
		function ($scope, $modalInstance, workingSummary, delta, HtmlFormatter) {
			$scope.formatted = HtmlFormatter.format(delta, workingSummary);

			$scope.close = function () {
				$modalInstance.close();
			};
		}
	])

	/**
	 * DiffFormater service
	 */
	.factory('DiffFormatter', ['Utils', function (Utils) {
		var DiffFormatter = {};

		/**
		 * Create a "delta" and the "diffs" object (used by the accordion) from two json
		 */
		DiffFormatter.create = function (working, state) {
			var instance = jsondiffpatch.create({
				objectHash: function (obj) {
					// Match objects by uniqueId or id
					return obj.uniqueId || obj.id;
				}
			});

			var left = Utils.cleanupAngularObject(working), right = Utils.cleanupAngularObject(state);
			var delta = instance.diff(right, left);

			return {
				'delta': delta,
				'diffs': this.buildDiff(delta, left)
			};
		};

		/**
		 * Create a 'diff' object containing catalogs/applications/universes id and their diff value (added, modified, deleted)
		 * See https://github.com/benjamine/jsondiffpatch/blob/master/docs/deltas.md
		 */
		DiffFormatter.buildDiff = function (delta, left) {
			var catalogs = left.catalogs, applications = left.applications;
			var diffs = {
				'applications': {},
				'catalogs': {},
				'universes': {}
			};

			// No differences
			if (!angular.isDefined(delta)) { return diffs; }

			// Catalogs
			if (angular.isDefined(delta.catalogs)) {
				for (var i in delta.catalogs) {
					var catalogDelta = delta.catalogs[i];

					// Changed catalog
					if (angular.isArray(catalogDelta)) {
						// A value is added
						if (catalogDelta.length === 1) { diffs.catalogs[catalogs[i].id] = 'added'; }
						// A value was replaced
						if (catalogDelta.length === 2) { diffs.catalogs[catalogs[i].id] = 'modified'; }
						// A value was deleted
						if (catalogDelta.length === 3 && catalogDelta[2] === 0) { diffs.catalogs[catalogDelta[0].id] = 'deleted'; }
					} else if (angular.isObject(catalogDelta)) {
						// Object with inner changes
						diffs.catalogs[catalogs[i].id] = 'modified';
					}
				}
			}

			// Applications
			if (angular.isDefined(delta.applications)) {
				for (var j in delta.applications) {
					var applicationDelta = delta.applications[j];

					// Changed application
					if (angular.isArray(applicationDelta)) {
						if (applicationDelta.length === 1) { diffs.applications[applications[j].id] = 'added'; }
						if (applicationDelta.length === 2) { diffs.applications[applications[j].id] = 'modified'; }
						if (applicationDelta.length === 3 && applicationDelta[2] === 0) { diffs.applications[applicationDelta[0].id] = 'deleted'; }
					} else if (angular.isObject(applicationDelta)) {
						// Inner changes
						for (var k in applicationDelta) {
							// Universes
							if (k === 'universes') {
								diffs.applications[applications[j].id] = 'modified';
								var universes = applicationDelta[k];

								for (var l in universes) {
									var universeDelta = universes[l];

									if (angular.isArray(universeDelta)) {
										if (universeDelta.length === 1) {
											diffs.universes[universeDelta[0].uniqueId] = 'added';
										}
										if (universeDelta.length === 3 && universeDelta[2] === 0) {
											diffs.universes[universeDelta[0].uniqueId] = 'deleted';
										}
									} else if (angular.isObject(universeDelta)) {
										// Inner changes in universe
										var universe = applications[j][k][l];
										diffs.universes[universe.uniqueId] = 'modified';
									}
								}
							} else {
								// Inner changes in application
								diffs.applications[applications[j].id] = 'modified';
							}
						}
					}
				}
			}

			return diffs;
		};

		return DiffFormatter;
	}])

	/**
	 * HtmlFormatter
	 * Builds a visual diff
	 */
	.factory('HtmlFormatter', function () {
		var getObjectKeys = typeof Object.keys === 'function' ?
			function (obj) {
				return Object.keys(obj);
			} : function (obj) {
				var names = [];
				for (var property in obj) {
					if (obj.hasOwnProperty(property)) {
						names.push(property);
					}
				}
				return names;
			};

		var trimUnderscore = function (str) {
			if (str.substr(0, 1) === '_') {
				return str.slice(1);
			}
			return str;
		};

		var arrayKeyToSortNumber = function (key) {
			if (key === '_t') {
				return -1;
			} else {
				if (key.substr(0, 1) === '_') {
					return parseInt(key.slice(1), 10);
				} else {
					return parseInt(key, 10) + 0.1;
				}
			}
		};

		var arrayKeyComparer = function (key1, key2) {
			return arrayKeyToSortNumber(key1) - arrayKeyToSortNumber(key2);
		};

		var HtmlFormatter = {};

		HtmlFormatter.format = function (delta, left) {
			var context = {};
			this.prepareContext(context);
			this.recurse(context, delta, left);
			return this.finalize(context);
		};

		HtmlFormatter.prepareContext = function (context) {
			context.buffer = [];
			context.out = function () {
				this.buffer.push.apply(this.buffer, arguments);
			};
		};

		HtmlFormatter.typeFormattterNotFound = function (context, deltaType) {
			throw new Error('cannot format delta type: ' + deltaType);
		};

		HtmlFormatter.typeFormattterErrorFormatter = function (context, err) {
			return err.toString();
		};

		HtmlFormatter.formatValue = function (context, value) {
			context.out('<pre>' + JSON.stringify(value, null, 2) + '</pre>');
		};

		HtmlFormatter.finalize = function (context) {
			if (angular.isArray(context.buffer)) {
				return context.buffer.join('');
			}
		};

		HtmlFormatter.recurse = function (context, delta, left, key, leftKey, movedFrom, isLast) {
			if (typeof delta === 'undefined' && typeof key === 'undefined') {
				return undefined;
			}
			var type = this.getDeltaType(delta, movedFrom);
			var nodeType = type === 'Node' ? (delta._t === 'a' ? 'array' : 'object') : '';

			if (typeof key !== 'undefined') {
				this.nodeBegin(context, key, leftKey, type, nodeType, isLast);
			} else {
				this.rootBegin(context, type, nodeType);
			}

			var typeFormattter;
			try {
				typeFormattter = this['format' + type] || this.typeFormattterNotFound(context, type);
				typeFormattter.call(this, context, delta, left, key, leftKey, movedFrom);
			} catch (err) {
				this.typeFormattterErrorFormatter(context, err, delta, left, key, leftKey, movedFrom);
				if (typeof console !== 'undefined' && console.error) {
					console.error(err.stack);
				}
			}

			if (typeof key !== 'undefined') {
				this.nodeEnd(context, key, leftKey, type, nodeType, isLast);
			} else {
				this.rootEnd(context, type, nodeType);
			}
		};

		HtmlFormatter.formatDeltaChildren = function (context, delta, left) {
			var self = this;
			this.forEachDeltaKey(delta, left, function (key, leftKey, movedFrom, isLast) {
				self.recurse(context, delta[key], left ? left[leftKey] : undefined,
					key, leftKey, movedFrom, isLast);
			});
		};

		HtmlFormatter.forEachDeltaKey = function (delta, left, fn) {
			var keys = getObjectKeys(delta);
			var arrayKeys = delta._t === 'a';
			var moveDestinations = {};
			var name;
			if (typeof left !== 'undefined') {
				for (name in left) {
					if (typeof delta[name] === 'undefined' &&
						((!arrayKeys) || typeof delta['_' + name] === 'undefined')) {
						keys.push(name);
					}
				}
			}
			// look for move destinations
			for (name in delta) {
				var value = delta[name];
				if (angular.isArray(value) && value[2] === 3) {
					moveDestinations[value[1].toString()] = value[0];
					if (this.includeMoveDestinations !== false) {
						if ((typeof left === 'undefined') &&
							(typeof delta[value[1]] === 'undefined')) {
							keys.push(value[1].toString());
						}
					}
				}
			}
			if (arrayKeys) {
				keys.sort(arrayKeyComparer);
			} else {
				keys.sort();
			}
			for (var index = 0, length = keys.length; index < length; index++) {
				var key = keys[index];
				if (arrayKeys && key === '_t') { continue; }
				var leftKey = arrayKeys ?
					(typeof key === 'number' ? key : parseInt(trimUnderscore(key), 10)) :
					key;
				var isLast = (index === length - 1);
				fn(key, leftKey, moveDestinations[leftKey], isLast);
			}
		};

		HtmlFormatter.getDeltaType = function (delta, movedFrom) {
			if (typeof delta === 'undefined') {
				if (typeof movedFrom !== 'undefined') {
					return 'Movedestination';
				}
				return 'Unchanged';
			}
			if (angular.isArray(delta)) {
				if (delta.length === 1) { return 'Added'; }
				if (delta.length === 2) { return 'Modified'; }
				if (delta.length === 3 && delta[2] === 0) { return 'Deleted'; }
				if (delta.length === 3 && delta[2] === 2) { return 'Textdiff'; }
				if (delta.length === 3 && delta[2] === 3) { return 'Moved'; }
			} else if (typeof delta === 'object') {
				return 'Node';
			}
			return 'Unknown';
		};

		HtmlFormatter.parseTextDiff = function (value) {
			var output = [];
			var lines = value.split('\n@@ ');
			for (var i = 0, l = lines.length; i < l; i++) {
				var line = lines[i];
				var lineOutput = {
					pieces: []
				};
				var location = /^(?:@@ )?[-+]?(\d+),(\d+)/.exec(line).slice(1);
				lineOutput.location = {
					line: location[0],
					chr: location[1]
				};
				var pieces = line.split('\n').slice(1);
				for (var pieceIndex = 0, piecesLength = pieces.length; pieceIndex < piecesLength; pieceIndex++) {
					var piece = pieces[pieceIndex];
					if (!piece.length) { continue; }
					var pieceOutput = { type: 'context' };
					if (piece.substr(0, 1) === '+') {
						pieceOutput.type = 'added';
					} else if (piece.substr(0, 1) === '-') {
						pieceOutput.type = 'deleted';
					}
					pieceOutput.text = piece.slice(1);
					lineOutput.pieces.push(pieceOutput);
				}
				output.push(lineOutput);
			}
			return output;
		};

		var adjustArrows = function jsondiffpatchHtmlFormatterAdjustArrows(node) {
			node = node || document;
			var getElementText = function (el) {
				return el.textContent || el.innerText;
			};
			var eachByQuery = function (el, query, fn) {
				var elems = el.querySelectorAll(query);
				for (var i = 0, l = elems.length; i < l; i++) {
					fn(elems[i]);
				}
			};
			var eachChildren = function (el, fn) {
				for (var i = 0, l = el.children.length; i < l; i++) {
					fn(el.children[i], i);
				}
			};
			eachByQuery(node, '.jsondiffpatch-arrow', function (arrow) {
				var arrowParent = arrow.parentNode;
				var svg = arrow.children[0], path = svg.children[1];
				svg.style.display = 'none';
				var destination = getElementText(arrowParent.querySelector('.jsondiffpatch-moved-destination'));
				var container = arrowParent.parentNode;
				var destinationElem;
				eachChildren(container, function (child) {
					if (child.getAttribute('data-key') === destination) {
						destinationElem = child;
					}
				});
				if (!destinationElem) { return; }
				try {
					var distance = destinationElem.offsetTop - arrowParent.offsetTop;
					svg.setAttribute('height', Math.abs(distance) + 6);
					arrow.style.top = (- 8 + (distance > 0 ? 0 : distance)) + 'px';
					var curve = distance > 0 ?
						'M30,0 Q-10,' + Math.round(distance / 2) + ' 26,' + (distance - 4) :
						'M30,' + (-distance) + ' Q-10,' + Math.round(-distance / 2) + ' 26,4';
					path.setAttribute('d', curve);
					svg.style.display = '';
				} catch(err) {
					return;
				}
			});
		};

		HtmlFormatter.rootBegin = function (context, type, nodeType) {
			var nodeClass = 'jsondiffpatch-' + type +
				(nodeType ? ' jsondiffpatch-child-node-type-' + nodeType : '');
			context.out('<div class="jsondiffpatch-delta ' + nodeClass + '">');
		};

		HtmlFormatter.rootEnd = function (context) {
			context.out('</div>' + (context.hasArrows ?
				('<script type="text/javascript">setTimeout(' +
					adjustArrows.toString() +
					',10);</script>') : ''));
		};

		HtmlFormatter.nodeBegin = function (context, key, leftKey, type, nodeType) {
			var nodeClass = 'jsondiffpatch-' + type +
				(nodeType ? ' jsondiffpatch-child-node-type-' + nodeType : '');
			context.out('<li class="' + nodeClass + '" data-key="' + leftKey + '">' +
				'<div class="jsondiffpatch-property-name">' + leftKey + '</div>');
		};

		HtmlFormatter.nodeEnd = function (context) {
			context.out('</li>');
		};

		HtmlFormatter.formatUnchanged = function (context, delta, left) {
			if (typeof left === 'undefined') { return; }
			context.out('<div class="jsondiffpatch-value">');
			this.formatValue(context, left);
			context.out('</div>');
		};

		HtmlFormatter.formatMovedestination = function (context, delta, left) {
			if (typeof left === 'undefined') { return; }
			context.out('<div class="jsondiffpatch-value">');
			this.formatValue(context, left);
			context.out('</div>');
		};

		HtmlFormatter.formatNode = function (context, delta, left) {
			// recurse
			var nodeType = (delta._t === 'a') ? 'array': 'object';
			context.out('<ul class="jsondiffpatch-node jsondiffpatch-node-type-' + nodeType + '">');
			this.formatDeltaChildren(context, delta, left);
			context.out('</ul>');
		};

		HtmlFormatter.formatAdded = function (context, delta) {
			context.out('<div class="jsondiffpatch-value">');
			this.formatValue(context, delta[0]);
			context.out('</div>');
		};

		HtmlFormatter.formatModified = function (context, delta) {
			context.out('<div class="jsondiffpatch-value jsondiffpatch-left-value">');
			this.formatValue(context, delta[0]);
			context.out('</div>' +
				'<div class="jsondiffpatch-value jsondiffpatch-right-value">');
			this.formatValue(context, delta[1]);
			context.out('</div>');
		};

		HtmlFormatter.formatDeleted = function (context, delta) {
			context.out('<div class="jsondiffpatch-value">');
			this.formatValue(context, delta[0]);
			context.out('</div>');
		};

		HtmlFormatter.formatMoved = function (context, delta) {
			context.out('<div class="jsondiffpatch-value">');
			this.formatValue(context, delta[0]);
			context.out('</div><div class="jsondiffpatch-moved-destination">' + delta[1] + '</div>');

			// draw an SVG arrow from here to move destination
			context.out(
				/*jshint multistr: true */
				'<div class="jsondiffpatch-arrow" style="position: relative; left: -34px;">\
				<svg width="30" height="60" style="position: absolute; display: none;">\
				<defs>\
					<marker id="markerArrow" markerWidth="8" markerHeight="8" refx="2" refy="4"\
						   orient="auto" markerUnits="userSpaceOnUse">\
						<path d="M1,1 L1,7 L7,4 L1,1" style="fill: #339;" />\
					</marker>\
				</defs>\
				<path d="M30,0 Q-10,25 26,50" style="stroke: #88f; stroke-width: 2px; fill: none;\
				stroke-opacity: 0.5; marker-end: url(#markerArrow);"></path>\
				</svg>\
				</div>');
			context.hasArrows = true;
		};

		HtmlFormatter.formatTextdiff = function (context, delta) {
			context.out('<div class="jsondiffpatch-value">');
			this.formatTextDiffString(context, delta[0]);
			context.out('</div>');
		};

		HtmlFormatter.showUnchanged = function (show, node, delay) {
			var el = node || document.body;
			var prefix = 'jsondiffpatch-Unchanged-';
			var classes = {
				showing: prefix + 'showing',
				hiding: prefix + 'hiding',
				visible: prefix + 'visible',
				hidden: prefix + 'hidden',
			};
			var list = el.classList;
			if (!list) { return; }
			if (!delay) {
				list.remove(classes.showing);
				list.remove(classes.hiding);
				list.remove(classes.visible);
				list.remove(classes.hidden);
				if (show === false) {
					list.add(classes.hidden);
				}
				return;
			}
			if (show === false) {
				list.remove(classes.showing);
				list.add(classes.visible);
				setTimeout(function (){
					list.add(classes.hiding);
				}, 10);
			} else {
				list.remove(classes.hiding);
				list.add(classes.showing);
				list.remove(classes.hidden);
			}
			var intervalId = setInterval(function (){
				adjustArrows(el);
			}, 100);
			setTimeout(function (){
				list.remove(classes.showing);
				list.remove(classes.hiding);
				if (show === false) {
					list.add(classes.hidden);
					list.remove(classes.visible);
				} else {
					list.add(classes.visible);
					list.remove(classes.hidden);
				}
				setTimeout(function (){
					list.remove(classes.visible);
					clearInterval(intervalId);
				}, delay + 400);
			}, delay);
		};

		HtmlFormatter.hideUnchanged = function (node, delay) {
			return this.showUnchanged(false, node, delay);
		};

		return HtmlFormatter;
	});

})();