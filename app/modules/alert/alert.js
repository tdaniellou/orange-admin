(function () {
	'use strict';

	/**
	 * Alerts module
	 */
	angular.module('angularApp.alert', [])

	/**
	 * Alert service
	 */
	.factory('AlertService',
		['$rootScope', '$timeout',
		function ($rootScope, $timeout) {
			var AlertService = {}, options = {type: 'info', timeout: 5000};

			$rootScope.alerts = [];

			AlertService.add = function (type, message, timeout) {
				var alert;

				alert = {
					'msg': (message ? message.toString() : ''),
					'type': type ? type.toString() : options.type,
					'timeout': timeout ? parseInt(timeout, 10) : options.timeout
				};

				$rootScope.alerts.push(alert);

				// Close alert after 5 seconds
				$timeout(function () {
					AlertService.closeAlert(this);
				}, alert.timeout);
			};

			AlertService.closeAlert = function (index) {
				$rootScope.alerts.splice(index, 1);
			};

			AlertService.clearAll = function () {
				$rootScope.alerts = [];
			};

			return AlertService;
		}
	])

	/**
	 * Alerts directive
	 */
	.directive('flashMessages', function () {
		return {
			template: '<alert ng-repeat="alert in alerts" type="{{ alert.type }}" close="closeAlert($index)">{{ alert.msg }}</alert>',
			restrict: 'C'
		};
	});
})();