(function () {
	'use strict';

	/**
	 * Application module
	 */
	angular.module('angularApp.application', ['ngResource', 'appConfig'])

	/**
	 * Constant for the form
	 */
	.constant('ApplicationConstants', {
		// Input descriptions
		'id': 'Identifiant de l\'application (Doit être cohérent avec les URL utilisées par les applications embarquées)',
		'businessVersion': 'Version métier TVM',
		'licenceUrl': 'Radical de l\'URL de retrait de la licence pour l\'application. Si omis, utilisation de l\'URL de base de la VOD.',
		'levels': 'Liste des codes CSA autorisés (whitelist)',
		'deliveries': 'Liste des modes de livraison autorisés (whitelist)',
		'qualities': 'Liste des qualités interdites (blacklist)',
		'categories': 'Liste des catégories à filtrer',
		'highlights': 'Liste des types de mises en avant autorisés (whitelist)',
		// Models
		'businessVersionList': ['v1', 'v2', 'v3', 'v4'],
		'deliveriesModel': ['DOWNLOAD', 'STREAMING'],
		'levelsModel': ['1', '2', '3', '4', '5'],
		'qualitiesModel': [
			'HD', 'HD-SELL', 'HD-RENT',
			'DVD', 'DVD-SELL', 'DVD-RENT',
			'SD', 'SD-SELL', 'SD-RENT',
			'SMOOTH', 'SMOOTH-SELL', 'SMOOTH-RENT',
			'3D', '3D-SELL', '3D-RENT'
		],
		'highlightsModel': ['CATEGORY', 'PACKAGE', 'VIDEO', 'SEASON', 'LINK']
	})

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			// Applications
			.state('app.applications', {
				url: '/applications',
				templateUrl: 'modules/common/table-default.tpl.html',
				controller: 'ApplicationCtrl',
				resolve: {
					applications: ['ApplicationService',
						function (ApplicationService) {
							return ApplicationService.getAll();
						}
					],
					categories: ['CategoryService',
						function (CategoryService) {
							return CategoryService.getAll();
						}
					],
				}
			})
			// Create
			.state('app.applications.create', {
				url: '/new',
				templateUrl: 'modules/application/form-application.tpl.html',
				controller: 'ApplicationCreateCtrl'
			})
			// Duplicate
			.state('app.applications.duplicate', {
				url: '/duplicate/:id',
				templateUrl: 'modules/application/form-application.tpl.html',
				controller: 'ApplicationDuplicateCtrl',
				resolve: {
					application: ['$stateParams', 'ApplicationService',
						function ($stateParams, ApplicationService) {
							var appId = $stateParams.id;

							return ApplicationService.get(appId);
						}
					],
					selectedCategories: ['$q', 'application', 'CategoryService',
						function ($q, application, CategoryService) {
							var deferred = $q.defer(), selectedCategories = {}, promises = [];

							var storeCategory = function (category) {
								selectedCategories[category.id] = {
									'id': category.id,
									'name': category.name,
									'selected': true
								};
							};

							var filters = application.filters ? application.filters : [];

							// Check if the categories are defined
							if (filters.categories) {
								var categories = filters.categories.categories;

								angular.forEach(categories, function (categoryId) {
									var getPromise = CategoryService.get(categoryId);
									promises.push(getPromise);
								});
							}

							$q.all(promises).then(function (categories) {
								angular.forEach(categories, function (category) {
									storeCategory(category);
								});
								deferred.resolve(selectedCategories);
							});

							return deferred.promise;
						}
					],
					qualities: ['$q', 'application',
						function ($q, application) {
							var deferred = $q.defer(), qualities = [];

							var filters = application.filters ? application.filters : [];

							// Check if the qualities are defined
							if (filters.qualities) {
								// Format the application's qualities and store it in the "qualities" model
								var univQualities = filters.qualities.qualities;

								angular.forEach(univQualities, function (quality) {
									qualities.push(quality.value);
								});
							}
							deferred.resolve(qualities);

							return deferred.promise;
						}
					]
				}
			})
			// Edit
			.state('app.applications.edit', {
				url: '/edit/:id',
				templateUrl: 'modules/application/form-application.tpl.html',
				controller: 'ApplicationEditCtrl',
				resolve: {
					application: ['$stateParams', 'ApplicationService',
						function ($stateParams, ApplicationService) {
							var appId = $stateParams.id;

							return ApplicationService.get(appId);
						}
					],
					selectedCategories: ['$q', 'application', 'CategoryService',
						function ($q, application, CategoryService) {
							var deferred = $q.defer(), selectedCategories = {}, promises = [];

							var storeCategory = function (category) {
								selectedCategories[category.id] = {
									'id': category.id,
									'name': category.name,
									'selected': true
								};
							};

							var filters = application.filters ? application.filters : [];

							// Check if the categories are defined
							if (filters.categories) {
								var categories = filters.categories.categories;

								angular.forEach(categories, function (categoryId) {
									var getPromise = CategoryService.get(categoryId);
									promises.push(getPromise);
								});
							}

							$q.all(promises).then(function (categories) {
								angular.forEach(categories, function (category) {
									storeCategory(category);
								});
								deferred.resolve(selectedCategories);
							});

							return deferred.promise;
						}
					],
					qualities: ['$q', 'application',
						function ($q, application) {
							var deferred = $q.defer(), qualities = [];

							var filters = application.filters ? application.filters : [];

							// Check if the qualities are defined
							if (filters.qualities) {
								// Format the application's qualities and store it in the "qualities" model
								var univQualities = filters.qualities.qualities;

								angular.forEach(univQualities, function (quality) {
									qualities.push(quality.value);
								});
							}
							deferred.resolve(qualities);

							return deferred.promise;
						}
					]
				}
			})
			// View
			.state('app.applications.view', {
				url: '/view/:id',
				templateUrl: 'modules/application/view-application.tpl.html',
				controller: 'ApplicationViewCtrl',
				resolve: {
					applicationView: ['$stateParams', 'ApplicationService',
						function ($stateParams, ApplicationService) {
							var appId = $stateParams.id;

							return ApplicationService.getView(appId);
						}
					]
				}
			});
	}])

	/**
	 * Application resource
	 */
	.factory('ApplicationResource',
		['$resource', 'APPLICATIONS_RESOURCE_URL',
		function ($resource, APPLICATIONS_RESOURCE_URL) {
			return $resource(APPLICATIONS_RESOURCE_URL, {id: '@id'}, {
				'query': {method: 'GET', params: {'state': '@state'}, isArray: false},
				'get': {method: 'GET'},
				'create': {method: 'POST'},
				'update': {method: 'PUT'},
				'remove': {method: 'DELETE'}
			});
		}
	])

	/**
	 * Application service
	 */
	.factory('ApplicationService',
		['$q', 'ApplicationResource', 'CategoryService', 'ApplicationConstants',
		function ($q, ApplicationResource, CategoryService, ApplicationConstants) {
			var ApplicationService = {};

			/**
			 * Get every applications
			 * @method getAll
			 * @param {String} state
			 * @return promise
			 */
			ApplicationService.getAll = function (state) {
				var deferred = $q.defer();

				state = (typeof state !== 'undefined') ? state : 'WORK';

				ApplicationResource.query({'state': state}, function (data) {
					deferred.resolve(data.applications);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};


			/**
			 * Get every applications ids
			 * @method getAllIds
			 * @param {String} state
			 * @return promise
			 */
			ApplicationService.getAllIds = function (state) {
				var deferred = $q.defer();

				state = (typeof state !== 'undefined') ? state : 'WORK';

				ApplicationResource.query({'state': state}, function (data) {
					var applications = data.applications, ids = [];

					angular.forEach(applications, function (application) {
						ids.push(application.id);
					});

					deferred.resolve(ids);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Get a specified application
			 * @method get
			 * @param {String} applicationId
			 * @return promise
			 */
			ApplicationService.get = function (applicationId) {
				var deferred = $q.defer();

				ApplicationResource.get({'id': applicationId}, function (data) {
					var application = data.application;

					// Check for blacklisted filters instead of whitelisted
					var filters = application.filters;

					if (filters) {
						// CSA
						var csa = filters.csa && filters.csa.levels && filters.csa.levels.length > 0 ?
							ApplicationService.convertBlistToWlist(filters.csa, 'levels', 'levelsModel') : [];

						// Deliveries
						var deliveries = filters.deliveries && filters.deliveries.deliveries && filters.deliveries.deliveries.length > 0 ?
							ApplicationService.convertBlistToWlist(filters.deliveries, 'deliveries', 'deliveriesModel') : [];

						// Highlights
						var highlightItems = filters.highlightItems && filters.highlightItems.highlightItems && filters.highlightItems.highlightItems.length > 0 ?
							ApplicationService.convertBlistToWlist(filters.highlightItems, 'highlightItems', 'highlightsModel') : [];

						// Fix for applications created from a different adminTool
						application.filters.csa = {};
						application.filters.csa.levels = csa;
						application.filters.csa.listType = 'WHITELIST';

						application.filters.deliveries = {};
						application.filters.deliveries.deliveries = deliveries;
						application.filters.deliveries.listType = 'WHITELIST';

						application.filters.highlightItems = {};
						application.filters.highlightItems.highlightItems = highlightItems;
						application.filters.highlightItems.listType = 'WHITELIST';
					}

					deferred.resolve(application);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Format an application for the view
			 * Warning: Applications with whitelisted qualities/categories will be displayed as blacklisted
			 * @method convertToModel
			 * @param {Object} application
			 * @return promise
			 */
			ApplicationService.convertToModel = function (application) {
				var deferred = $q.defer();

				// Application filters
				var csa, deliveries, highlightItems, qualities, categories;

				// Temp vars for cleaning
				var applicationQualities, applicationCategories, qualitiesValues = [];

				var filters = application.filters ? application.filters : [];

				// CSA
				csa = filters.csa && filters.csa.levels.length > 0 ?
					this.convertBlistToWlist(filters.csa, 'levels', 'levelsModel') : [];

				// Deliveries
				deliveries = filters.deliveries && filters.deliveries.deliveries.length > 0 ?
					this.convertBlistToWlist(filters.deliveries, 'deliveries', 'deliveriesModel') : [];

				// Highlights
				highlightItems = filters.highlightItems && filters.highlightItems.highlightItems.length > 0 ?
					this.convertBlistToWlist(filters.highlightItems, 'highlightItems', 'highlightsModel') : [];

				// Qualities
				// Warning: Whitelist not supported
				applicationQualities = filters.qualities && filters.qualities.qualities.length > 0 ?
					filters.qualities.qualities : [];

				// Store the qualities' values
				angular.forEach(applicationQualities, function (quality) {
					qualitiesValues.push(quality.value);
				});

				qualities = qualitiesValues.length > 0 ? qualitiesValues : [];

				// Categories
				// Warning: Whitelist not supported
				applicationCategories = filters.categories ? filters.categories.categories : [];

				// Get the categories' name
				CategoryService.getNames(applicationCategories).then(function (categoriesNames) {
					categories = categoriesNames.length > 0 ? categoriesNames : [];

					var applicationView = {
						'id': application.id,
						'businessVersion': application.businessVersion,
						'filters': {
							'csa': csa,
							'deliveries': deliveries,
							'filteredQualities': qualities,
							'filteredCategories': categories,
							'highlightItems': highlightItems
						},
						'licenceUrl': application.licenceUrl
					};

					deferred.resolve(applicationView);
				});

				return deferred.promise;
			};

			/**
			 * Convert a blacklist filter to whitelist
			 * @param {Object} filter
			 * @param {String} filter's property
			 * @param {String} ApplicationConstants' property name
			 */
			ApplicationService.convertBlistToWlist = function (filter, property, constantProperty) {
				var wProperty = filter[property];

				if (filter.listType === 'BLACKLIST') {
					wProperty = angular.copy(ApplicationConstants[constantProperty]);
					for (var i in filter[property]) {
						var index = wProperty.indexOf(filter[property][i]);
						wProperty.splice(index, 1);
					}
				}

				return wProperty;
			};

			/**
			 * Get a specified application's view
			 * @method getView
			 * @param {String} applicationId
			 * @return promise
			 */
			ApplicationService.getView = function (applicationId) {
				var deferred = $q.defer();

				ApplicationService.get(applicationId).then(function (application) {
					if (application) {
						ApplicationService.convertToModel(application).then(function (applicationView) {
							deferred.resolve(applicationView);
						});
					}
				});

				return deferred.promise;
			};

			/**
			 * Get all applications views
			 * @method getAllViews
			 * @param {String} state
			 * @return promise
			 */
			ApplicationService.getAllViews = function (state) {
				var deferred = $q.defer();

				ApplicationService.getAll(state).then(function (applications) {
					var promises = [];

					angular.forEach(applications, function (application) {
						var viewPromise = ApplicationService.convertToModel(application);
						promises.push(viewPromise);
					});

					deferred.resolve($q.all(promises));
				});

				return deferred.promise;
			};

			/**
			 * Create an application
			 * @method create
			 * @param {Object} application
			 * @return promise
			 */
			ApplicationService.create = function (application) {
				var deferred = $q.defer();

				var applicationResource = new ApplicationResource({
					'id': application.id,
					'filters': application.filters,
					'businessVersion': application.businessVersion,
					'licenceUrl': application.licenceUrl
				});

				applicationResource.$create({'id':''}, function (success) {
					if (success.error !== undefined) {
						deferred.reject(success.error.message);
					} else {
						deferred.resolve('Nouvelle application enregistrée.');
					}
				}, function (error) {
					deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
				});

				return deferred.promise;
			};

			/**
			 * Update an application
			 * @method update
			 * @param {Object} application
			 * @return promise
			 */
			ApplicationService.update = function (application) {
				var deferred = $q.defer();

				ApplicationResource.update({'id': application.id}, application,
					function (success) {
						if (angular.isDefined(success.error)) {
							deferred.reject(success.error.message);
						} else {
							deferred.resolve('Application "' + application.id + '" modifiée.');
						}
					}, function (error) {
						deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
					}
				);

				return deferred.promise;
			};

			/**
			 * Delete an application (keyword "remove" instead of "delete" due to IE8 incompatibilities)
			 * @method remove
			 * @param {String} applicationId
			 * @return promise
			 */
			ApplicationService.remove = function (applicationId) {
				var deferred = $q.defer();

				ApplicationResource.remove({'id': applicationId}, function () {
					deferred.resolve('Application ' + applicationId + ' supprimée.');
				}, function (error) {
					deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
				});

				return deferred.promise;
			};

			return ApplicationService;
		}
	])

	/**
	 * Applications controller
	 * Displays the list of applications
	 */
	.controller('ApplicationCtrl',
		['$scope', '$state', '$stateParams', '$modal', 'ApplicationService', 'applications', 'categories', 'ApplicationConstants',
		function ($scope, $state, $stateParams, $modal, ApplicationService, applications, categories, ApplicationConstants) {
			$scope.pageTitle = 'Applications';
			$scope.pageAction = '';
			$scope.singleWord = /^\s*\w*\s*$/;

			// Select models
			$scope.businessVersionList = angular.copy(ApplicationConstants.businessVersionList);
			$scope.deliveriesModel = angular.copy(ApplicationConstants.deliveriesModel);
			$scope.levelsModel = angular.copy(ApplicationConstants.levelsModel);
			$scope.qualitiesModel = angular.copy(ApplicationConstants.qualitiesModel);
			$scope.highlightsModel = angular.copy(ApplicationConstants.highlightsModel);

			$scope.rows = applications;

			// Selected applications object
			$scope.checkboxes = {};

			// Initialize the checkboxes
			angular.forEach(applications, function (application) {
				if (application && application.id) {
					$scope.checkboxes[application.id] = {};
				}
			});

			// CSS classes for input validation
			$scope.validateState = function (ngModelController) {
				return {
					'has-error': ngModelController.$invalid && ngModelController.$dirty,
					'has-success': ngModelController.$valid && ngModelController.$dirty
				};
			};

			// Get the description for an input
			$scope.tooltipHelp = function (ngModelController) {
				return ApplicationConstants[ngModelController.$name];
			};

			// Get an input error description
			$scope.showError = function (ngModelController, error) {
				return ngModelController.$error[error];
			};

			// Open the delete prompt modal
			$scope.open = function (selectedApplications) {
				$modal.open({
					templateUrl: 'modules/common/modal-delete.tpl.html',
					controller: 'ApplicationDeleteCtrl',
					resolve: {
						Items : function () {
							return selectedApplications;
						}
					}
				});
			};

			// Cancel the form
			$scope.cancel = function () {
				$scope.redirect();
			};

			// Redirect to applications list
			$scope.redirect = function () {
				$state.go('app.applications', $stateParams, {reload: true});
			};
		}
	])

	/**
	 * Application create controller
	 * Create the application object, gets the resources for the form
	 * and saves a the new application when submitting the form.
	 */
	.controller('ApplicationCreateCtrl',
		['$scope', 'ApplicationService', 'AlertService', 'categories',
		function ($scope, ApplicationService, AlertService, categories) {
			$scope.$parent.pageAction = 'Création';

			// Models for the form
			$scope.application = {
				'id': '',
				'businessVersion': $scope.businessVersionList[0],
				'filters': {
					'csa': {
						'levels': angular.copy($scope.levelsModel),	// Every csa levels are preselected
						'listType': 'WHITELIST'
					},
					'deliveries': {
						'deliveries': angular.copy($scope.deliveriesModel),
						'listType': 'WHITELIST'
					},
					'qualities': {
						'qualities': [],
						'listType': 'BLACKLIST'
					},
					'categories': {
						'categories': [],
						'listType': 'BLACKLIST'
					},
					'highlightItems': {
						'highlightItems': angular.copy($scope.highlightsModel),	// Every highlights are preselected
						'listType': 'WHITELIST'
					}
				},
				'licenceUrl': ''
			};

			$scope.categories = categories;
			$scope.selectedCategories = {};
			$scope.qualities = [];

			// Process the form
			$scope.submit = function () {
				var application = $scope.application;

				// Qualities
				var qualities = [];
				for (var i = 0; i < $scope.qualities.length; i++) {
					qualities.push({'value': $scope.qualities[i]});
				}

				// Selected categories
				var selectedCategories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						var id = category.id;
						selectedCategories.push(id);
					}
				}

				application.filters.qualities.qualities = qualities;
				application.filters.categories.categories = selectedCategories;

				// POST the new application
				ApplicationService.create(application).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * Application edit controller
	 * Display the form with the application's values,
	 * gets the required resource for the form
	 * and saves the modified values when the form is submitted
	 */
	.controller('ApplicationEditCtrl',
		['$scope', 'ApplicationService', 'AlertService', 'application', 'categories', 'selectedCategories', 'qualities',
		function ($scope, ApplicationService, AlertService, application, categories, selectedCategories, qualities) {
			$scope.$parent.pageAction = 'Modification';

			$scope.application = application;
			$scope.categories = categories;
			$scope.selectedCategories = selectedCategories;
			$scope.qualities = qualities;

			$scope.submit = function () {

				// Qualities
				var qualities = [];
				for (var i = 0; i < $scope.qualities.length; i++) {
					qualities.push({'value': $scope.qualities[i]});
				}

				$scope.application.filters.qualities = {'qualities': [], 'listType': 'BLACKLIST'};
				// Save the new 'qualities' array
				$scope.application.filters.qualities.qualities = qualities;

				// Selected categories
				var univCategories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						univCategories.push(category.id);
					}
				}

				$scope.application.filters.categories = {'categories': [], 'listType': 'BLACKLIST'};
				// Save the new 'categories' array
				$scope.application.filters.categories.categories = univCategories;

				// Application resource
				var application = $scope.application;

				// Update the application
				ApplicationService.update(application).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * View application controller
	 * Displays an application's information
	 */
	.controller('ApplicationViewCtrl',
		['$scope', 'applicationView',
		function ($scope, applicationView) {
			$scope.$parent.pageAction = 'Visualisation';
			$scope.applicationView = applicationView;
		}
	])

	/**
	 * Application duplicate controller
	 * Display the form with an application's values,
	 * gets the required resource for the form
	 * and create the duplicated application when the form is submitted
	 */
	.controller('ApplicationDuplicateCtrl',
		['$scope', 'ApplicationService', 'AlertService', 'application', 'categories', 'selectedCategories', 'qualities',
		function ($scope, ApplicationService, AlertService, application, categories, selectedCategories, qualities) {
			$scope.$parent.pageAction = 'Duplication';

			$scope.application = application;

			// Clear the id
			$scope.application.id = '';

			$scope.qualities = qualities;
			$scope.categories = categories;
			$scope.selectedCategories = selectedCategories;

			$scope.submit = function () {
				var application = $scope.application;

				// Qualities
				var selectedQualities = [];
				for (var i = 0; i < $scope.qualities.length; i++) {
					selectedQualities.push({'value': $scope.qualities[i]});
				}

				// Selected categories
				var selectedCategories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						var id = category.id;
						selectedCategories.push(id);
					}
				}

				application.filters.qualities.qualities = selectedQualities;
				application.filters.categories.categories = selectedCategories;

				ApplicationService.create(application).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * Application delete controller
	 * Deletes application(s) when the modal prompt is accepted
	 */
	.controller('ApplicationDeleteCtrl',
		['$scope', '$state', '$stateParams', '$modalInstance', 'ApplicationService', 'Items', 'AlertService', '$q',
		function ($scope, $state, $stateParams, $modalInstance, ApplicationService, Items, AlertService, $q) {
			// Bind the items for the template
			$scope.modalTitle = 'Supprimer des applications'; // Modal
			$scope.items = Items;

			var closeAndRedirect = function () {
				$modalInstance.close();
				$state.go('app.applications', $stateParams, {reload: true});
			};

			// Accept the modal
			$scope.accept = function () {
				var promises = [];

				// Delete every selected applications
				for (var i = 0; i < Items.length; i++) {
					var deletePromise = ApplicationService.remove(Items[i]);
					promises.push(deletePromise);
				}

				$q.all(promises).then(function (success) {
					for (var i = 0; i < success.length; i++) {
						AlertService.add('success', success[i]);
					}
					closeAndRedirect();
				}, function (error) {
					AlertService.add('danger', error);
					closeAndRedirect();
				});
			};

			// Dismiss the modal
			$scope.dismiss = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);

})();