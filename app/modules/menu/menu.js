(function () {
	'use strict';

	angular.module('angularApp.menu', [])

	/**
	 * Main menu controller
	 * Builds the sidebar menu from the "links" object
	 */
	.controller('MainMenuCtrl',
		['$scope', function ($scope) {
			$scope.links = [
				{
					'name': 'Configuration de travail',
					'url': 'app.summary',
					'links': [
						{
							'name': 'Catalogues',
							'url': 'app.catalogs'
						},
						{
							'name': 'Applications',
							'url': 'app.applications',
							'links': [
								{
									'name': 'Univers',
									'url': 'app.universes'
								}
							]
						}
					]
				},
				{
					'name': 'Catégories RiGHTv',
					'url': 'app.categories'
				},
				{
					'name': 'Publication',
					'url': 'app.publications'
				}/*,
				{
					'name': 'Gestion des médias',
					'url': 'app.medias'
				}*/
			];
		}
	])

	/**
	 * Navigation directive
	 * Renders recursively the menu
	 * TODO: replace 'ui-sref-active' to 'ui-sref-active-equals' on ui-router next release
	 */
	.directive('navigation', ['$compile', function ($compile) {
		return {
			restrict: 'AE',
			replace: true,
			scope: {
				links: '='
			},
			template: '<ul class="nav">' +
						'<li ng-repeat="link in links" ui-sref-active="active">' +
							'<a ui-sref="{{ link.url }}">' +
								'{{ link.name }}' +
							'</a>' +
							'<navigation ng-if="link.links && link.links.length > 0" links="link.links"></navigation>' +
						'</li>' +
					'</ul>',
			compile: function (element) {
				var contents = element.contents().remove(), compiled;

				return function (scope, element) {
					if (!compiled) {
						compiled = $compile(contents);
					}

					compiled(scope, function (clone) {
						element.append(clone);
					});
				};
			}
		};

	}]);

})();