(function () {
	'use strict';

	angular.module('angularApp.category', ['ngResource', 'appConfig'])

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			// Categories
			.state('app.categories', {
				url: '/categories',
				templateUrl: 'modules/category/categories.tpl.html',
				controller: 'CategoriesCtrl',
				resolve: {
					categories: ['CategoryService',
						function (CategoryService) {
							return CategoryService.getAll();
						}
					]
				}
			});
	}])

	/**
	 * Categories resource
	 */
	.factory('CategoryResource',
		['$resource', 'CATEGORIES_RESOURCE_URL',
		function ($resource, CATEGORIES_RESOURCE_URL) {
			return $resource(CATEGORIES_RESOURCE_URL, {id: '@id'}, {
				query: {method: 'GET', isArray: false}
			});
		}
	])

	/**
	 * Category service
	 */
	.factory('CategoryService', ['$q', 'CategoryResource', 'CatalogService',
		function ($q, CategoryResource, CatalogService) {

			var CategoryService = {};
			var categoriesMap = {};
			var isCategoriesMapBuilt = false, isCategoriesMapBuilding = false;

			/**
			 * Build a map of every categories recursively
			 * @method buildMap
			 * @param {Object} categories
			 * @param {Object} categoriesMap
			 */
			var buildMap = function (categories, categoriesMap) {
				angular.forEach(categories, function (category) {
					categoriesMap[category.id] = category;
					if (category.categories && category.categories.length > 0) {
						buildMap(category.categories, categoriesMap);
					}
				});
			};

			/**
			 * recurseBuildMap
			 * Same as buildMap, but returns a promise and resolve when the map is built
			 */
			var recurseBuildMap = function (categories, categoriesMap, deferred) {
				if (!deferred) {
					deferred = $q.defer();
				}

				angular.forEach(categories, function (category) {
					categoriesMap[category.id] = category;
					if (category.categories && category.categories.length > 0) {
						recurseBuildMap(category.categories, categoriesMap, deferred);
					} else {
						deferred.resolve();
					}
				});

				return deferred.promise;
			};

			// TODO: Find a better name and complete this
			var buildMap2 = function () {
				var deferred = $q.defer(), promises = [];

				CategoryService.getAll().then(function (categoriesByCatalog) {
					for (var catalogId in categoriesByCatalog) {
						var buildPromise = recurseBuildMap(categoriesByCatalog[catalogId].categories, categoriesMap);
						promises.push(buildPromise);
					}

					$q.all(promises).then(function () {
						deferred.resolve(categoriesMap);
						isCategoriesMapBuilt = true;
					});
				});

				return deferred.promise;
			};

			/**
			 * getMap
			 * Builds the map and returns it when ready
			 * TODO: Finish this (find how to avoid multiple calls at the same time)
			 */
			var getMap = function () {
				var deferred = $q.defer();

				if (!isCategoriesMapBuilt && !isCategoriesMapBuilding) {
					isCategoriesMapBuilding = true;
					buildMap2().then(function () {
						isCategoriesMapBuilt = true;
					});
				} else {
					if (isCategoriesMapBuilt) {
						deferred.resolve(categoriesMap);
					}
				}

				return deferred.promise;
			};

			/**
			 * Get the catalogs and their categories
			 * @method getAll
			 * @return promise
			 */
			CategoryService.getAll = function () {
				var deferred = $q.defer();

				// Get the catalogs name
				CatalogService.getAll().then(function (catalogs) {
					var temp = [], categories = {}, promises = [];

					// Save the names
					for (var i = 0; i < catalogs.length; i++) {
						if (catalogs[i].id) {
							temp[i] = catalogs[i].id;
						}
					}

					// Get the categories for each catalogs
					angular.forEach(temp, function (catalog) {
						var deferredForEach = $q.defer();

						promises.push(deferredForEach.promise);
						// Async get on categories
						CategoryResource.query({id: catalog}, function (data) {
							// Store the data in the "categories" object
							var category = {'id': catalog, 'categories': data.categories};
							categories[catalog] = category;
							deferredForEach.resolve();
						});
					});

					$q.all(promises).then(function () {
						deferred.resolve(categories);
					});
				});

				return deferred.promise;
			};

			/**
			 * Get a category
			 * @method get
			 * @param {Integer} categoryId
			 * @return promise
			 */
			CategoryService.get = function (categoryId) {
				var deferred = $q.defer();

				/** TODO: Replace this **/
				if (!isCategoriesMapBuilt) {
					CategoryService.getAll().then(function (categoriesByCatalog) {
						for (var catalogId in categoriesByCatalog) {
							buildMap(categoriesByCatalog[catalogId].categories, categoriesMap);
						}
						deferred.resolve(categoriesMap[categoryId]);
						isCategoriesMapBuilt = true;
					});
				} else {
					deferred.resolve(categoriesMap[categoryId]);
				}

				/** By this when ready **/
				// getMap().then(function () {
				// 	deferred.resolve(categoriesMap[categoryId]);
				// });

				return deferred.promise;
			};

			/**
			 * Get a category from a catalog id
			 * @method getByCatalog
			 * @param {Integer} catalogId
			 * @return promise
			 */
			CategoryService.getByCatalog = function (catalogId) {
				var deferred = $q.defer();
				var categories = {};

				CategoryResource.query({id: catalogId}, function (data) {
					var category = {'id': catalogId, 'categories': data.categories};
					categories[catalogId] = category;
					deferred.resolve(categories);
				});

				return deferred.promise;
			};

			/**
			 * Get categories from their ids
			 * @method getByIds
			 * @param {Array} categoriesIds
			 * @return promise
			 */
			CategoryService.getByIds = function (categoriesIds) {
				var deferred = $q.defer();
				var categories = {};
				var promises = [];

				angular.forEach(categoriesIds, function (categoryId) {
					var deferredCategory = $q.defer();

					promises.push(deferredCategory.promise);
					CategoryService.get(categoryId).then(function (data) {
						categories[categoryId] = data;
						deferredCategory.resolve();
					});
				});

				$q.all(promises).then(function () {
					deferred.resolve(categories);
				});

				return deferred.promise;
			};

			/**
			 * Get the categories names from their ids
			 * @method getNames
			 * @param {Array} categoriesIds
			 * @return promise
			 */
			CategoryService.getNames = function (categoriesIds) {
				var deferred = $q.defer();
				var categoriesNames = [];
				var promises = [];

				angular.forEach(categoriesIds, function (categoryId) {
					var deferredCategories = $q.defer();
					promises.push(deferredCategories.promise);

					CategoryService.get(categoryId).then(function (category) {
						if (category) {
							categoriesNames.push(category.name);
						}
						deferredCategories.resolve();
					});
				});

				$q.all(promises).then(function () {
					categoriesNames.sort();
					deferred.resolve(categoriesNames);
				});

				return deferred.promise;

			};

			// CategoryService service
			return CategoryService;
		}
	])

	/**
	 * CategoryService controller
	 * Displays every categories
	 */
	.controller('CategoriesCtrl',
		['$scope', '$modal', 'categories',
		function ($scope, $modal, categories) {
			$scope.data = categories;

			// Reload categories modal
			$scope.open = function () {
				$modal.open({
					templateUrl: 'modules/category/modal-categories.tpl.html',
					controller: 'CategoriesReloadCtrl'
				});
			};
		}
	])

	/**
	 * Reload categories controller
	 * ModalInstance
	 * Lists the catalogs and reload the categories of the selected catalogs
	 */
	.controller('CategoriesReloadCtrl',
		['$scope', '$state', '$stateParams', '$modalInstance', '$http', '$q', 'CatalogService', 'AlertService', 'CATEGORIES_RELOAD_URL',
		function ($scope, $state, $stateParams, $modalInstance, $http, $q, CatalogService, AlertService, CATEGORIES_RELOAD_URL) {

			CatalogService.getAll().then(function (catalogs) {
				$scope.catalogs = [];
				$scope.selectedCatalogs = {};

				for (var i = 0; i < catalogs.length; i++) {
					var catalog = catalogs[i].id;

					$scope.catalogs.push(catalog);
					$scope.selectedCatalogs[catalog] = true;
				}
			});

			// Confirm the modal
			$scope.ok = function () {
				var promises = [];
				for (var catalog in $scope.selectedCatalogs) {
					if ($scope.selectedCatalogs[catalog] === true) {
						var promise = $http.get(CATEGORIES_RELOAD_URL + catalog + '?forceReload=true');
						promises.push(promise);
					}
				}

				$q.all(promises).then(function () {
					AlertService.add('success', 'Le rechargement des catégories à réussi.');
					$modalInstance.close();
				}, function () {
					AlertService.add('danger', 'Erreur lors du rechargement des catégories.');
					$modalInstance.close();
				});
				$state.go($state.current, $stateParams, {reload: true});
			};

			$scope.cancel = function () {
				$modalInstance.dismiss();
			};
		}
	])

	/**
	 * Category directive
	 * Renders every categories
	 * (Not used)
	 */
	.directive('categories', function () {
		return {
			restrict: 'E', // Element
			replace: true,
			scope: {
				categories: '=' // Bind the data to the attibute
			},
			template: '<ul><category ng-repeat="category in categories" category="category"></category></ul>'
		};
	})

	/**
	 * Category directive
	 * Renders every nested categories
	 * (Not used)
	 */
	.directive('category',
		['$compile',
		function ($compile) {
			return {
				restrict: 'E',
				replace: true,
				scope: {
					category: '='
				},
				template: '<li><strong>{{ category.name }}</strong> ({{ category.id }})</li>',
				link: function (scope, element, attrs) {
					if (angular.isArray(scope.category.categories)) {
						var subCategory = '<categories categories="category.categories"></categories>';
						$compile(subCategory)(scope, function (cloned) {
							element.append(cloned);
						});
					}
				}
			};
		}
	]);

})();