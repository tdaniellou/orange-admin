(function () {
	'use strict';

	/**
	 * Module
	 */
	angular.module('angularApp.publication', [])

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			// Publications
			.state('app.publications', {
				url: '/publication',
				templateUrl: 'modules/publication/publication.tpl.html',
				controller: 'PublicationCtrl',
				resolve: {
					workingSummary: ['SummaryService', function (SummaryService) {
						return SummaryService.getSummary();
					}],
					prodSummary: ['SummaryService', function (SummaryService) {
						return SummaryService.getSummary('PROD');
					}]
				}
			});
	}])

	/**
	 * Publication controller
	 * Displays the WORK & PROD summaries and the publish button
	 */
	.controller('PublicationCtrl',
		['$scope', '$modal', 'workingSummary', 'prodSummary', 'DiffFormatter', 'Utils',
		function ($scope, $modal, workingSummary, prodSummary, DiffFormatter, Utils) {
			$scope.workingSummary = workingSummary;
			$scope.prodSummary = prodSummary;

			// Open/close model for the accordion
			$scope.opened = {
				'applications': {},
				'catalogs': {},
				'universes': {}
			};

			$scope.displayLegend = true;

			// Models for summary settings directive
			$scope.workPrefix = '';
			$scope.statePrefix = '';

			// Get the diff between the two summaries
			var diff = DiffFormatter.create($scope.workingSummary, $scope.prodSummary);

			$scope.delta = diff.delta;
			$scope.diffs = diff.diffs;

			// Modal
			$scope.open = function () {
				$modal.open({
					templateUrl: 'modules/common/modal-confirm.tpl.html',
					controller: 'PublicationModalCtrl'
				});
			};

			$scope.viewDiff = function () {
				$modal.open({
					templateUrl: 'modules/summary/modal-jsondiff.tpl.html',
					controller: 'JsonDiffCtrl',
					resolve: {
						workingSummary: function () {
							return Utils.cleanupAngularObject($scope.workingSummary);
						},
						delta: function () {
							return $scope.delta;
						}
					}
				});
			};
		}
	])

	/**
	 * Publication ModalInstance controller
	 * Publishes applications, catalogs and universes (PUBLICATIONS_URLS defined in app.js)
	 */
	.controller('PublicationModalCtrl',
		['$scope', '$state', '$stateParams', '$modalInstance', '$q', '$http', 'AlertService', 'PUBLICATIONS_URLS',
		function ($scope, $state, $stateParams, $modalInstance, $q, $http, AlertService, PUBLICATIONS_URLS) {
			$scope.message = 'Voulez vous vraiment publier les "Catalogues", "Applications" et "Univers" ?';

			/**
			 * Publish method
			 * Pass through the PUBLICATIONS_URL object and GET on the URLS
			 */
			$scope.publish = function () {
				var promises = [];
				var modules = PUBLICATIONS_URLS;

				angular.forEach(modules, function (module) {
					var deferred = $q.defer();
					promises.push(deferred.promise);

					$http.get(module.url)
						.success(function () {
							deferred.resolve();
						})
						.error(function () {
							deferred.reject('Erreur lors de la plublication des ' + module.name);
						});
				});

				$q.all(promises).then(function () {
					AlertService.add('success', 'La publication a réussi.');
				}, function (error) {
					AlertService.add('danger', 'La publication a échoué (Cause : '+ error +').');
				});
			};

			// Modal confirm
			$scope.confirm = function () {
				$scope.publish();
				$modalInstance.close();
				$state.go($state.current, $stateParams, {reload: true});
			};

			// Modal cancel
			$scope.cancel = function () {
				$modalInstance.dismiss();
			};
		}
	]);
})();