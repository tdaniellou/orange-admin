(function () {
	'use strict';

	angular.module('angularApp.medias', [])

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			.state('app.medias', {
				url: '/medias',
				templateUrl: 'modules/medias/medias.tpl.html'
			});
	}])

	.directive('jstree', function () {
		return {
			restrict: 'AE',
			scope: {
				selectedNode: '=',

			},
			link: function (scope, element, attrs) {
				// Needs "$" (jQuery)
				var treeElem = element;
				var tree = treeElem.jstree();

				scope.selectedNode = {
					'id': '1',
					'text': 'Text 1',
				};
			}
		};
	});

})();