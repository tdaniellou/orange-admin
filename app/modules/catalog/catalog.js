(function () {
	'use strict';

	/**
	 * Catalog module
	 */
	angular.module('angularApp.catalog', ['ngResource', 'appConfig'])

	/**
	 * Constant containing the form's input description
	 */
	.constant('catalogsInputHelp', {
		'id': 'Ce champ correspond à l\'identifiant du catalogue',
		'type': 'Type',
		'searchOriginId': 'Catalogue et contexte de recherche'
	})

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		$stateProvider
			// Catalogs
			.state('app.catalogs', {
				url: '/catalogs',
				templateUrl: 'modules/common/table-default.tpl.html',
				controller: 'CatalogCtrl',
				resolve: {
					catalogs: ['CatalogService',
						function (CatalogService) {
							return CatalogService.getAll();
						}
					]
				}
			})
			// Create
			.state('app.catalogs.create', {
				url: '/new',
				templateUrl: 'modules/catalog/form-catalog.tpl.html',
				controller: 'CatalogCreateCtrl'
			})
			// Duplicate
			.state('app.catalogs.duplicate', {
				url: '/duplicate/:id',
				templateUrl: 'modules/catalog/form-catalog.tpl.html',
				controller: 'CatalogDuplicateCtrl',
				resolve: {
					catalog: ['$stateParams', 'CatalogService',
						function ($stateParams, CatalogService) {
							var catalogId = $stateParams.id;
							return CatalogService.get(catalogId);
						}
					]
				}
			})
			// Edit
			.state('app.catalogs.edit', {
				url: '/edit/:id',
				templateUrl: 'modules/catalog/form-catalog.tpl.html',
				controller: 'CatalogEditCtrl',
				resolve: {
					catalog: ['$stateParams', 'CatalogService',
						function ($stateParams, CatalogService) {
							var catalogId = $stateParams.id;
							return CatalogService.get(catalogId);
						}
					]
				}
			})
			// View
			.state('app.catalogs.view', {
				url: '/view/:id',
				templateUrl: 'modules/catalog/view-catalog.tpl.html',
				controller: 'CatalogViewCtrl',
				resolve: {
					catalogView: ['$stateParams', 'CatalogService',
						function ($stateParams, CatalogService) {
							var catalogId = $stateParams.id;
							return CatalogService.get(catalogId);
						}
					]
				}
			});
	}])

	/**
	 * Catalog resource for GET/POST/PUT/DELETE requests
	 */
	.factory('CatalogResource',
		['$resource', 'CATALOGS_RESOURCE_URL',
		function ($resource, CATALOGS_RESOURCE_URL) {
			return $resource(CATALOGS_RESOURCE_URL, {id: '@id'}, {
				'query': {method: 'GET', params: {'state': '@state'}, isArray: false},
				'get': {method: 'GET'},
				'create': {method: 'POST'},
				'update': {method: 'PUT'},
				'remove': {method: 'DELETE'}
			});
		}
	])

	/**
	 * Catalog service
	 */
	.factory('CatalogService',
		['CatalogResource', '$q',
		function (CatalogResource, $q) {
			var CatalogService = {};

			/**
			 * Get all the catalogs
			 * @method getAll
			 * @param {String} state
			 * @return promise
			 */
			CatalogService.getAll = function (state) {
				var deferred = $q.defer();

				state = (typeof state !== 'undefined') ? state : 'WORK';

				CatalogResource.query({'state': state}, function (data) {
					deferred.resolve(data.catalogs);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Get a catalog
			 * @method get
			 * @param {String} catalogId
			 * @return promise
			 */
			CatalogService.get = function (catalogId) {
				var deferred = $q.defer();

				CatalogResource.get({'id': catalogId}, function (data) {
					deferred.resolve(data.catalog);
				});

				return deferred.promise;
			};

			/**
			 * Create a catalog
			 * @method create
			 * @param {Object} catalog
			 * @return promise
			 */
			CatalogService.create = function (catalog) {
				var deferred = $q.defer();

				var catalogResource = new CatalogResource({
					'id': catalog.id,
					'type': catalog.type,
					'name': catalog.name,
					'searchOriginId': catalog.searchOriginId
				});

				catalogResource.$create({'id': ''}, function (success) {
						// Check the response from the server
						if (success.error !== undefined) {
							deferred.reject(success.error.message);
						} else {
							deferred.resolve('Nouveau catalogue enregistré.');
						}
					}, function (error) {
						deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
					}
				);

				return deferred.promise;
			};

			/**
			 * Update a catalog
			 * @method update
			 * @param {Object} catalog
			 * @return promise
			 */
			CatalogService.update = function (catalog) {
				var deferred = $q.defer();

				CatalogResource.update({'id': catalog.id}, catalog, function (success) {
					if (angular.isDefined(success.error)) {
						deferred.reject(success.error.message);
					} else {
						deferred.resolve('Catalogue "' + catalog.id + '" modifié.');
					}
				}, function (error) {
					deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
				});

				return deferred.promise;
			};

			/**
			 * Delete a catalog
			 * @method remove
			 * @param {String} catalogId
			 * @return promise
			 */
			CatalogService.remove = function (catalogId) {
				var deferred = $q.defer();

				CatalogResource.remove({'id': catalogId}, function () {
					deferred.resolve('Catalogue ' + catalogId + ' supprimé.');
				}, function (error) {
					deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
				});

				return deferred.promise;
			};

			return CatalogService;
		}
	])

	/**
	 * Catalogs controller.
	 * Displays the catalogs and the form to create one
	 */
	.controller('CatalogCtrl',
		['$scope', '$state', '$stateParams', '$modal', 'catalogs', 'catalogsInputHelp',
		function ($scope, $state, $stateParams, $modal, catalogs, catalogsInputHelp) {
			// Page title
			$scope.pageTitle = 'Catalogues';
			$scope.pageAction = '';

			$scope.checkboxes = {};

			// Single word pattern for validation
			$scope.singleWord = /^\s*\w*\s*$/;

			// Initialize the checkboxes
			angular.forEach(catalogs, function (catalog) {
				if (catalog.id) {
					$scope.checkboxes[catalog.id] = {};
				}
			});

			// Get the catalogs
			$scope.rows = catalogs;

			// CSS classes for input validation
			$scope.validateState = function (ngModelController) {
				return {
					'has-error': ngModelController.$invalid && ngModelController.$dirty,
					'has-success': ngModelController.$valid && ngModelController.$dirty
				};
			};

			// Get the description for an input
			$scope.tooltipHelp = function (ngModelController) {
				return catalogsInputHelp[ngModelController.$name];
			};

			// Get an input error description
			$scope.showError = function (ngModelController, error) {
				return ngModelController.$error[error];
			};

			// Cancel the catalog form
			$scope.cancel = function () {
				$scope.redirect();
			};

			// Open the delete dialog
			$scope.open = function (selectedCatalogs) {
				$modal.open({
					templateUrl: 'modules/common/modal-delete.tpl.html',
					controller: 'CatalogDeleteCtrl',
					// Inject the selected items in the CatalogDeleteCtrl
					resolve: {
						Items : function () {
							return selectedCatalogs;
						}
					}
				});
			};

			// Redirect to the Catalogs list
			$scope.redirect = function () {
				$state.go('app.catalogs', $stateParams, {reload: true});
			};
		}
	])

	/**
	 * Catalog create controller
	 * Displays the form and create a catalog when the form is submitted
	 */
	.controller('CatalogCreateCtrl',
		['$scope', '$modal', 'CatalogService', 'AlertService',
		function ($scope, $modal, CatalogService, AlertService) {
			$scope.$parent.pageAction = 'Création';

			// Catalog object
			$scope.catalog = {
				'id': '',
				'type': '',
				'name': '',
				'searchOriginId': ''
			};

			// Catalog create method
			$scope.submit = function () {
				var catalog = $scope.catalog;

				CatalogService.create(catalog).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * Catalog delete controller
	 * ModalInstance
	 * Deletes category(ies) when the modal prompt is accepted
	 */
	.controller('CatalogDeleteCtrl',
		['$scope', '$state', '$stateParams', '$modalInstance', '$q', 'CatalogService', 'Items', 'AlertService',
		function ($scope, $state, $stateParams, $modalInstance, $q, CatalogService, Items, AlertService) {
			// Bind the items for the template
			$scope.modalTitle = 'Supprimer des catalogues'; // Modal message
			$scope.items = Items;

			// Accept the modal
			$scope.accept = function () {
				var promises = [];

				var closeAndRedirect = function () {
					$modalInstance.close();
					$state.go('app.catalogs', $stateParams, {reload: true});
				};

				// Delete every selected catalogs
				for (var i = 0; i < Items.length; i++) {
					var deletePromise = CatalogService.remove(Items[i]);
					promises.push(deletePromise);
				}

				$q.all(promises).then(function (success) {
					for (var i = 0; i < success.length; i++) {
						AlertService.add('success', success[i]);
					}
					closeAndRedirect();
				}, function (error) {
					AlertService.add('danger', error);
					closeAndRedirect();
				});
			};

			// Dismiss the modal
			$scope.dismiss = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	])

	/**
	 * Catalog edit controller
	 * Fill the catalog form with the selected catalog and save the new values when the form is submitted
	 */
	.controller('CatalogEditCtrl',
		['$scope', 'CatalogService', 'AlertService', 'catalog',
		function ($scope, CatalogService, AlertService, catalog) {
			$scope.$parent.pageAction = 'Modification';

			$scope.catalog = catalog;

			// Submit the form
			$scope.submit = function () {
				// Get the new values from the form
				var catalog = $scope.catalog;

				CatalogService.update(catalog).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * Catalogs view controller
	 * Displays a catalog's informations
	 */
	.controller('CatalogViewCtrl',
		['$scope', 'catalogView',
		function ($scope, catalogView) {
			$scope.$parent.pageAction = 'Visualisation';
			$scope.catalogView = catalogView;
		}
	])

	/**
	 * Duplicate catalogs controller
	 * Fill the form with a catalog values and save the new catalog when the form is submitted
	 */
	.controller('CatalogDuplicateCtrl',
		['$scope', 'CatalogService', 'AlertService', 'catalog',
		function ($scope, CatalogService, AlertService, catalog) {
			$scope.$parent.pageAction = 'Duplication';
			$scope.catalog = catalog;

			// Clear catalog id
			$scope.catalog.id = '';

			// Submit the form
			$scope.submit = function () {
				var catalog = $scope.catalog;

				CatalogService.create(catalog).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	]);

})();