(function () {
	'use strict';

	angular.module('angularApp.universe', ['ngResource', 'appConfig'])

	/**
	 * Constant containing the form's input description
	 */
	.constant('UniverseConstants', {
		'id': 'Identifiant de l\'univers (Ne contient pas d\'espace)',
		'vodApplicationId': 'Application',
		'name': 'Nom de l\'univers',
		'catalogId': 'Catalogue',
		'categories': 'Catégories de l\'univers',
		'mergingMode': 'Merging mode de l\'univers'
	})

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		// Universes
		$stateProvider.state('app.universes', {
			url: '/universes',
			templateUrl: 'modules/universe/table-universe.tpl.html',
			controller: 'UniverseCtrl',
			resolve: {
				applications: ['ApplicationService',
					function (ApplicationService) {
						return ApplicationService.getAll();
					}
				],
				catalogs: ['CatalogService',
					function (CatalogService) {
						return CatalogService.getAll();
					}
				],
				categories: ['catalogs', 'CategoryService',
					function (catalogs, CategoryService) {
						// Preselect the first catalog
						var catalog = catalogs[0];

						return CategoryService.getByCatalog(catalog.id);
					}
				],
				universes: ['UniverseService',
					function (UniverseService) {
						return UniverseService.getAll();
					}
				]
			}
		})
		// Create
		.state('app.universes.create', {
			url: '/new',
			templateUrl: 'modules/universe/form-universe.tpl.html',
			controller: 'UniverseCreateCtrl'
		})
		// Edit
		.state('app.universes.edit', {
			url: '/edit/:id',
			templateUrl: 'modules/universe/form-universe.tpl.html',
			controller: 'UniverseEditCtrl',
			resolve: {
				universe: ['$stateParams', 'UniverseService',
					function ($stateParams, UniverseService) {
						var id = $stateParams.id;
						var split = id.split('-');
						var vodApplicationId = split[0];
						var univId = split[1];

						return UniverseService.get(univId, vodApplicationId);
					}
				],
				selectedCategories: ['$q', 'universe', 'CategoryService',
					function ($q, universe, CategoryService) {
						var deferred = $q.defer();
						var univCategories = universe.categories.categories, selectedCategories = {};

						if (univCategories) {
							CategoryService.getByIds(univCategories)
								.then(function (categories) {

									angular.forEach(categories, function (category) {
										if (category) {
											selectedCategories[category.id] = {
												'id': category.id,
												'name': category.name,
												'ancestors': category.ancestors,
												'categories': category.categories,
												'selected': true
											};
										}
									});

								});
						}
						deferred.resolve(selectedCategories);

						return deferred.promise;
					}
				]
			}
		})
		// View
		.state('app.universes.view', {
			url: '/view/:id',
			templateUrl: 'modules/universe/view-universe.tpl.html',
			controller: 'UniverseViewCtrl',
			resolve: {
				universeView: ['$stateParams', 'UniverseService',
					function ($stateParams, UniverseService) {
						var id = $stateParams.id;
						var split = id.split('-');
						var vodApplicationId = split[0], univId = split[1];

						return UniverseService.getView(univId, vodApplicationId);
					}
				]
			}
		})
		// Duplicate
		.state('app.universes.duplicate', {
			url: '/duplicate/:id',
			templateUrl: 'modules/universe/form-universe.tpl.html',
			controller: 'UniversesDuplicateCtrl',
			resolve: {
				universe: ['$stateParams', 'UniverseService',
					function ($stateParams, UniverseService) {
						var id = $stateParams.id;
						var split = id.split('-');
						var vodApplicationId = split[0];
						var univId = split[1];

						return UniverseService.get(univId, vodApplicationId);
					}
				],
				selectedCategories: ['$q', 'universe', 'CategoryService',
					function ($q, universe, CategoryService) {
						var deferred = $q.defer();
						var univCategories = universe.categories.categories, selectedCategories = {};

						if (univCategories) {
							CategoryService.getByIds(univCategories)
								.then(function (categories) {

									angular.forEach(categories, function (category) {
										if (category) {
											selectedCategories[category.id] = {
												'id': category.id,
												'name': category.name,
												'ancestors': category.ancestors,
												'categories': category.categories,
												'selected': true
											};
										}
									});

								});
						}
						deferred.resolve(selectedCategories);

						return deferred.promise;
					}
				]
			}
		});
	}])

	/**
	 * Universe resource
	 */
	.factory('UniverseResource',
		['$resource', 'UNIVERSES_RESOURCE_URL',
		function ($resource, UNIVERSES_RESOURCE_URL) {
			return $resource(UNIVERSES_RESOURCE_URL, {id: '@id', applicationId: '@applicationId'}, {
				'query': {method: 'GET', isArray: false},
				'get': {method: 'GET'},
				'create': {method: 'POST'},
				'update': {method: 'PUT'},
				'remove': {method: 'DELETE'}
			});
		}
	])

	/**
	 * Universe service
	 */
	.factory('UniverseService',
		['$q', '$http', 'UniverseResource', 'CategoryService',
		function ($q, $http, UniverseResource, CategoryService) {
			var UniverseService = {};

			/**
			 * Get every universes
			 * @method getAll
			 * @return promise
			 */
			UniverseService.getAll = function () {
				var deferred = $q.defer();

				UniverseResource.query(function (data) {
					deferred.resolve(data.universes);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Get every universes ids
			 * @method getAllIds
			 * @return promise
			 */
			UniverseService.getAllIds = function () {
				var deferred = $q.defer();
				var ids = [];

				UniverseResource.query(function (data) {
					var universes = data.universes;

					angular.forEach(universes, function (universe) {
						ids.push(universe.vodApplicationId + '-' + universe.id);
					});

					deferred.resolve(ids);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Get a universe
			 * @method get
			 * @param {String} universeId
			 * @param {String} applicationId
			 * @return promise
			 */
			UniverseService.get = function (universeId, applicationId) {
				var deferred = $q.defer();

				UniverseResource.get({'id': universeId, 'applicationId': applicationId}, function (data) {
					var universe = data.universe;

					// Quickfix: Case if merging mode is null
					if (!universe.mergingMode) {
						universe.mergingMode = 'NONE';
					}

					deferred.resolve(universe);
				}, function (error) {
					deferred.reject(error);
				});

				return deferred.promise;
			};

			/**
			 * Get a universe from an application id
			 * @method getByApplication
			 * @param {String} applicationId
			 * @param {String} state (optional)
			 * @return promise
			 */
			UniverseService.getByApplication = function (applicationId, state) {
				var deferred = $q.defer();

				state = (typeof state !== 'undefined') ? state : 'WORK';

				// TODO: Store the url in the app.js
				$http.get('/vode-admin/v1/universes', {params: {'applicationId': applicationId, 'state': state}})
					//.success(function (data, status, headers, config) {
					.success(function (data) {
						deferred.resolve(data.universes);
					});

				return deferred.promise;
			};

			/**
			 * Get an application's universes view
			 * @method getByApplication
			 * @param {String} applicationId
			 * @param {String} state (optional)
			 * @return promise
			 */
			UniverseService.getViewsByApplication = function (applicationId, state) {
				var deferred = $q.defer();

				state = (typeof state !== 'undefined') ? state : 'WORK';

				UniverseService.getByApplication(applicationId, state)
					.then(function (universes) {
						var promises = [];

						angular.forEach(universes, function (universe) {
							var cleanPromise = UniverseService.convertToModel(universe);
							promises.push(cleanPromise);
						});

						$q.all(promises).then(function (result) {
							deferred.resolve(result);
						});
					});

				return deferred.promise;
			};

			/**
			 * Format a universe for the view
			 * @method convertToModel
			 * @param {Object} universe
			 * @return promise
			 */
			UniverseService.convertToModel = function (universe) {
				var deferred = $q.defer();

				var categories = universe.categories ? universe.categories.categories : [];

				var universeView = {
					'id': universe.id,
					'vodApplicationId': universe.vodApplicationId,
					'uniqueId': universe.vodApplicationId + '-' + universe.id,
					'name': universe.name,
					'catalogId': universe.catalogId,
					'categories': [],
					'mergingMode': universe.mergingMode ? universe.mergingMode : 'NONE'
				};

				CategoryService.getNames(categories).then(function (categoriesNames) {
					universeView.categories = categoriesNames;

					deferred.resolve(universeView);
				});

				return deferred.promise;
			};

			/**
			 * Get a universe formatted for the view
			 * @method getView
			 * @param {String} universeId
			 * @param {String} applicationId
			 * @return promise
			 */
			UniverseService.getView = function (universeId, applicationId) {
				var deferred = $q.defer();

				UniverseService.get(universeId, applicationId).then(function (universe) {
					UniverseService.convertToModel(universe).then(function (universeView) {
						deferred.resolve(universeView);
					});
				});

				return deferred.promise;
			};

			/**
			 * Create a universe
			 * @method create
			 * @param {Object} universe
			 * @return promise
			 */
			UniverseService.create = function (universe) {
				var deferred = $q.defer();

				var universeResource = new UniverseResource({
					'id': universe.id,
					'catalogId': universe.catalogId,
					'name':  universe.name,
					'mergingMode': universe.mergingMode,
					'categories': {
						'categories': universe.categories
					},
					'vodApplicationId':  universe.vodApplicationId
				});

				universeResource.$create({'id': ''}, function (success) {
						// Check the response from the server
						if (success.error !== undefined) {
							deferred.reject(success.error.message);
						} else {
							deferred.resolve('Nouvel univers enregistré.');
						}
					}, function (error) {
						deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
					}
				);

				return deferred.promise;
			};

			/**
			 * Update a universe
			 * @method update
			 * @param {Object} universe
			 * @return promise
			 */
			UniverseService.update = function (universe) {
				var deferred = $q.defer();

				UniverseResource.update({'id': universe.id, 'applicationId': universe.vodApplicationId}, universe,
					function (success) {
						if (angular.isDefined(success.error)) {
							deferred.reject(success.error.message);
						} else {
							deferred.resolve('Univers "' + universe.id + '" modifié.');
						}
					}, function (error) {
						deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
					});

				return deferred.promise;
			};

			/**
			 * Delete a universe
			 * @method remove
			 * @param {String} universeId
			 * @param {String} applicationId
			 * @return promise
			 */
			UniverseService.remove = function (universeId, applicationId) {
				var deferred = $q.defer();

				UniverseResource.remove({'id': universeId, 'applicationId': applicationId}, function () {
					deferred.resolve('Univers "' + applicationId + '-' + universeId + '" supprimé.');
				}, function (error) {
					deferred.reject('Erreur ' + error.status + ': ' + error.statusText);
				});

				return deferred.promise;
			};

			return UniverseService;
		}
	])

	/**
	 * Universes controller
	 * Displays the list of universes
	 */
	.controller('UniverseCtrl',
		['$scope', '$state', '$stateParams', '$modal', 'universes', 'applications', 'catalogs', 'categories', 'UniverseConstants',
		function ($scope, $state, $stateParams, $modal, universes, applications, catalogs, categories, UniverseConstants) {
			$scope.pageTitle = 'Univers';
			$scope.pageAction = '';

			$scope.singleWordPattern = /^\s*\w*\s*$/;

			// Checkboxes values
			$scope.checkboxes = {};

			$scope.rows = {};

			angular.forEach(universes, function (universe) {
				$scope.rows[universe.vodApplicationId] = [];
				$scope.checkboxes[universe.vodApplicationId] = {};
			});

			angular.forEach(universes, function (universe) {
				var id = universe.vodApplicationId + '-' + universe.id;
				$scope.rows[universe.vodApplicationId].push({'id': id, 'name': universe.id});
				$scope.checkboxes[universe.vodApplicationId][id] = false;
			});

			// CSS classes for input validation
			$scope.validateState = function (ngModelController) {
				return {
					'has-error': ngModelController.$invalid && ngModelController.$dirty,
					'has-success': ngModelController.$valid && ngModelController.$dirty
				};
			};

			// Get the input description
			$scope.tooltipHelp = function (ngModelController) {
				return UniverseConstants[ngModelController.$name];
			};

			// Get an input error description
			$scope.showError = function (ngModelController, error) {
				return ngModelController.$error[error];
			};

			// Disable the merginMode input if the selected application's businessVersion is not 'v1'
			$scope.disableMergingModeInput = function (selectedApplicationId) {
				if (selectedApplicationId) {
					for (var i in applications) {
						var application = applications[i];
						if (application.id === selectedApplicationId) {
							return application.businessVersion !== 'v1';
						}
					}
				}

				return true;
			};

			// Redirect to universes list
			$scope.redirect = function () {
				$state.go('app.universes', $stateParams, {reload: true});
			};

			// Cancel the form
			$scope.cancel = function () {
				$scope.redirect();
			};

			// Open the delete dialog
			$scope.open = function (selectedItems) {
				$modal.open({
					templateUrl: 'modules/common/modal-delete.tpl.html',
					controller: 'UniverseDeleteCtrl',
					// Inject the selected items in the UniverseDeleteCtrl
					resolve: {
						Items : function () {
							return selectedItems;
						}
					}
				});
			};
		}
	])

	/**
	 * Universe create controller
	 * Create a universe object, get the resources for the form
	 * and save the new universe when the form is submitted.
	 */
	.controller('UniverseCreateCtrl',
		['$scope', 'AlertService', 'UniverseService', 'CategoryService', 'applications', 'catalogs', 'categories',
		function ($scope, AlertService, UniverseService, CategoryService, applications, catalogs, categories) {
			$scope.$parent.pageAction = 'Création';

			$scope.applications = applications;
			$scope.catalogs = catalogs;
			$scope.categories = categories;
			$scope.selectedCategories = {};

			// Universe object
			$scope.universe = {
				'animationCategoryId': '',
				'catalogId': catalogs[0].id,
				'categories': {
					'categories': []
				},
				'id': '',
				'mergingMode': 'NONE',
				'name': '',
				'vodApplicationId': ''
			};

			// Submit the form
			$scope.submit = function () {
				var universe = $scope.universe;

				// Categories
				universe.categories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						var id = category.id;
						universe.categories.push(id);
					}
				}

				// Create the universe from the form values
				UniverseService.create(universe).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};

			// Reload categories when the catalog is changed
			$scope.$watch('universe.catalogId', function (newValue, oldValue) {
				if (newValue !== oldValue) {
					var catalogId = $scope.universe.catalogId;

					CategoryService.getByCatalog(catalogId).then(function (categories) {
						$scope.categories = categories;
					});
				}
			}, true);
		}
	])

	/**
	 * Universe edit controller
	 * Displays the form with the universe's values,
	 * get the required resources for the form
	 * and saves the modified values when the form is submitted
	 */
	.controller('UniverseEditCtrl',
		['$scope', 'UniverseService', 'CategoryService', 'AlertService', 'universe', 'applications', 'catalogs', 'categories', 'selectedCategories',
		function ($scope, UniverseService, CategoryService, AlertService, universe, applications, catalogs, categories, selectedCategories) {
			$scope.$parent.pageAction = 'Modification';

			$scope.applications = applications;
			$scope.catalogs = catalogs;
			$scope.categories = categories;
			$scope.universe = universe;
			$scope.selectedCategories = selectedCategories;

			// TODO: Duplicated code
			// Reload categories on catalog change
			$scope.$watch('universe.catalogId', function (newValue, oldValue) {
				if (newValue !== oldValue) {
					var catalogId = $scope.universe.catalogId;

					CategoryService.getByCatalog(catalogId).then(function (categories) {
						$scope.categories = categories;
					});
				}
			});

			// TODO Check merging mode = NONE if the application's businessVersion is 'v1' ?

			// Submit the form
			$scope.submit = function () {
				// Categories
				var categories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						var categoryId = category.id;
						categories.push(categoryId);
					}
				}

				var universe = $scope.universe;
				universe.categories.categories = categories;

				UniverseService.update(universe).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});
			};
		}
	])

	/**
	 * View universe controller
	 * Displays a universe's information
	 */
	.controller('UniverseViewCtrl',
		['$scope', 'universeView',
		function ($scope, universeView) {
			$scope.$parent.pageAction = 'Visualisation';

			// Universe identifier for action buttons
			$scope.univId = universeView.uniqueId;
			$scope.universeView = universeView;
		}
	])

	/**
	 * Universe duplicate controller
	 * Displays the form with a universe's values,
	 * gets the required resources for the form
	 * and saves the modified values when the form is submitted
	 */
	.controller('UniversesDuplicateCtrl',
		['$scope', 'UniverseService', 'AlertService', 'CategoryService', 'universe', 'applications', 'catalogs', 'categories', 'selectedCategories',
		function ($scope, UniverseService, AlertService, CategoryService, universe, applications, catalogs, categories, selectedCategories) {
			$scope.$parent.pageAction = 'Duplication';

			$scope.applications = applications;
			$scope.catalogs = catalogs;
			$scope.categories = categories;
			$scope.universe = universe;
			$scope.selectedCategories = selectedCategories;

			// Clear universe's id
			$scope.universe.id = '';

			// TODO: Duplicated code
			$scope.$watch('universe.catalogId', function (newValue, oldValue) {
				if (newValue !== oldValue) {
					var catalogId = $scope.universe.catalogId;

					CategoryService.getByCatalog(catalogId).then(function (categories) {
						$scope.categories = categories;
					});
				}
			});

			// Submit the form
			$scope.submit = function () {
				var universe = $scope.universe;

				// Categories
				var categories = [];
				for (var key in $scope.selectedCategories) {
					var category = $scope.selectedCategories[key];
					if (category.selected === true) {
						var id = category.id;
						categories.push(id);
					}
				}

				universe.categories = categories;

				UniverseService.create(universe).then(function (success) {
					AlertService.add('success', success);
					$scope.redirect();
				}, function (error) {
					AlertService.add('danger', error);
					$scope.redirect();
				});

			};
		}
	])

	/**
	 * Delete universe controller
	 * ModalInstance
	 * Deletes universe(s) when the modal dialog is accepted
	 */
	.controller('UniverseDeleteCtrl',
		['$scope', '$state', '$stateParams', '$modalInstance', '$q', 'UniverseService', 'Items', 'AlertService',
		function ($scope, $state, $stateParams, $modalInstance, $q, UniverseService, Items, AlertService) {
			// Bind the items for the template
			$scope.modalTitle = 'Supprimer des univers';
			$scope.items = Items;

			var closeAndRedirect = function () {
				$modalInstance.close();
				$state.go('app.universes', $stateParams, {reload: true});
			};

			// Accept the modal
			$scope.accept = function () {
				var promises = [];

				// Delete every selected universes
				for (var i = 0; i < Items.length; i++) {
					var split = Items[i].split('-');
					var vodApplicationId = split[0];
					var univId = split[1];

					// Check if the universe exists and delete it
					var deletePromise = UniverseService.remove(univId, vodApplicationId);
					promises.push(deletePromise);
				}

				$q.all(promises).then(function (success) {
					for (var i = 0; i < success.length; i++) {
						AlertService.add('success', success[i]);
					}
					closeAndRedirect();
				}, function (error) {
					AlertService.add('danger', error);
					closeAndRedirect();
				});
			};

			// Dismiss the modal
			$scope.dismiss = function () {
				$modalInstance.dismiss('cancel');
			};
		}
	]);
})();