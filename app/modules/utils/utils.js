(function () {
	'use strict';

	var utils = angular.module('angularApp.utils', []);

	/**
	 * TriIndeterminate directive
	 * Easy to use "indeterminate" property
	 * Example: <input type "checkbox" tri-indeterminate="true">
	 */
	utils.directive('triIndeterminate', [function () {
		return {
			compile: function (elem, attrs) {
				if (!attrs.type || attrs.type.toLowerCase() !== 'checkbox') {
					return angular.noop;
				}

				return function (scope, element, attributes) {
					scope.$watch(attributes.triIndeterminate, function (newValue) {
						element[0].indeterminate = !!newValue; // Ensures boolean value
					});
				};
			}
		};
	}]);

	/**
	 * TriStateCheckbox directive
	 * Display a triState checkbox that selects/deselects every checkboxes
	 * passed in the isolated scope
	 */
	utils.directive('triStateCheckbox', function () {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				checkboxes: '='
			},
			template: '<input type="checkbox" ng-model="checkAll" tri-indeterminate="indeterminate" ng-change="masterChange()">',
			link: function (scope, element, attrs) {
				scope.checkAll = false;
				scope.indeterminate = false;

				scope.masterChange = function () {
					if(scope.checkAll) {
						angular.forEach(scope.checkboxes, function (value, key) {
							scope.checkboxes[key] = true;
						});
					} else {
						angular.forEach(scope.checkboxes, function (value, key) {
							scope.checkboxes[key] = false;
						});
					}
				};

				scope.$watch('checkboxes', function (values) {
					var checked = 0, unchecked = 0, total = Object.keys(values).length;

					angular.forEach(values, function (value) {
						if (value === true) {
							checked++;
						} else {
							unchecked++;
						}
					});

					// Master's indeterminate value
					scope.indeterminate = (checked !== 0 && unchecked !== 0);
					// Master's checked value
					scope.checkAll = (checked === total && total > 0);
				}, true);
			},
		};
	});

	/**
	 * ActionButtons directive
	 * Displays the action buttons (Create/Duplicate/View/Edit/Delete)
	 * and disable them when needed
	 */
	utils.directive('actionButtons', [function () {
		return {
			replace: true,
			templateUrl: 'modules/utils/action-buttons.html',
			restrict: 'C',
			link: function (scope, element, attrs) {

				scope.$watch('checkboxes', function (values) {
					scope.selected = [];
					for (var checkboxId in scope.checkboxes) {
						var checkbox = scope.checkboxes[checkboxId];

						if (angular.isObject(checkbox)) {
							// Universes
							for (var universe in checkbox) {
								if (checkbox[universe] === true) {
									// Push the universe if it's not already in the array
									if (scope.selected.indexOf(universe) === -1) {
										scope.selected.push(universe);
									}
								}
							}
						} else {
							// Applications/Catalogs
							for (var id in values) {
								if (values[id] === true) {
									if (scope.selected.indexOf(id) === -1) {
										scope.selected.push(id);
									}
								}
							}
						}
					}

					// Can edit only if one checkbox is selected
					scope.selectOneFlag = (scope.selected.length === 1);
					// Can delete only if one checkbox or more are selected
					scope.selectMultipleFlag = (scope.selected.length > 0);
				}, true);
			}
		};
	}]);

	/**
	 * Utils service
	 * Random functions available for the angularApp
	 */
	utils.factory('Utils', function () {
		var Utils = {};

		// Remove angular's variables (ex: $$hashKey)
		Utils.cleanupAngularObject = function (object) {
			var cleaned = angular.copy(object);

			if (cleaned instanceof Array) {
				for (var i = 0; i < cleaned.length; i++) {
					this.cleanupAngularObject(cleaned[i]);
				}
			}
			else if (cleaned instanceof Object) {
				for (var property in cleaned) {
					if (/^\$+/.test(property)) {
						delete cleaned[property];
					}
					else {
						this.cleanupAngularObject(cleaned[property]);
					}
				}
			}

			return cleaned;
		};

		return Utils;
	});
})();