(function () {
	'use strict';

	var multiSelectModule = angular.module('multiSelect', []);

	// TODO: Recursive search filter
	multiSelectModule.filter('normalizedFilter', function () {

		var normalizeForSearch = function (string) {
			function filter(character) {
				switch (character) {
					case 'á':
					case 'à':
					case 'ã':
					case 'â':
					case 'ä':
						return 'a';

					case 'é':
					case 'ê':
					case 'è':
					case 'ё':
						return 'e';

					case 'ï':
					case 'í':
						return 'i';

					case 'ó':
					case 'õ':
					case 'ô':
					case 'ö':
						return 'o';

					case 'ú':
					case 'û':
					case 'ü':
						return 'u';

					case 'ç':
						return 'c';

					default:
						return character;
				}
			}

			var normalized = '', i, l;
			string = string.toLowerCase();
			for (i = 0, l = string.length; i < l; i = i + 1) {
				normalized = normalized + filter(string.charAt(i));
			}

			return normalized;
		};

		var deepSearch = function (item, criteria) {
			if (item.name) {
				var text = normalizeForSearch(item.name);
				var search = normalizeForSearch(criteria);

				if (text.indexOf(search) > -1) {
					return true;
				}

				return deepSearch(item.categories);
			}

			return false;
		};

		var normalizedFilter = function (items, filterCriteria) {
			var filtered = [];

			angular.forEach(items, function (item) {
				if (deepSearch(item, filterCriteria)) {
					filtered.push(item);
				}
			});

			return filtered;
		};

		return normalizedFilter;
	});

	/**
	 * Multi Select directive
	 * Build and displays a tree from an "input" object
	 * and binds the selected items to an "output" array
	 * Params:
	 *	multi-select-input:						Object	Input data
	 *	multi-select-output:					Object	Output data
	 *	multi-select-read-only:					Boolean (optional) ReadOnly mode (default: false)
	 *	multi-select-children-auto:				Boolean	(optional) Automatically select children when selected default: true
	 *	multi-select-auto-expand-children:		Boolean	(optional) Automatically expand children when selected default: true
	 *	multi-select-auto-expand-sub-children:	Boolean	(optional) Automatically expand subchildren when selected default: true
	 *	multi-select-auto-disable-family:		Boolean (optional) Automatically disable family when selected default: false
	 *	multi-select-children-property:			String	(optional) Input data's children property default: 'children'
	 *	multi-select-parent-property:			String	(optional) Input data's parent property default: 'parent'
	 */
	multiSelectModule.directive('multiSelect', function () {
		return {
			restrict: 'A',	// Attribute
			replace: true,
			scope: {
				input: '=multiSelectInput',
				output: '=multiSelectOutput',
				autoSelectChildren: '@multiSelectChildrenAuto',
				autoExpandChildren: '@multiSelectAutoExpandChildren',
				autoExpandSubChildren: '@multiSelectAutoExpandSubChildren',
				autoDisableFamily: '@multiSelectAutoDisableFamily',
				$childProperty: '@multiSelectChildrenProperty',
				$parentProperty: '@multiSelectParentProperty',
				readOnly: '@multiSelectReadOnly'
			},
			templateUrl: 'modules/multiSelect/multiSelect.tpl.html',
			link: function (scope, element, attrs) {

				// Directive's optional parameters
				var autoSelectChildren = scope.autoSelectChildren !== 'false' ? true : false;
				var autoExpandChildren = scope.autoExpandChildren !== 'false' ? true : false;
				var autoExpandSubChildren = scope.autoExpandSubChildren !== 'false' ? true : false;
				var autoDisableFamily = scope.autoDisableFamily !== 'true' ? false : true;
				var $childProperty = scope.$childProperty ? scope.$childProperty : 'children';
				var $parentProperty = scope.$parentProperty ? scope.$parentProperty : 'parents';
				var readOnly = scope.readOnly !== 'true' ? false : true;

				if (!angular.isDefined(scope.input)) {
					console.log('Multi-Select: Input is undefined.');
					return;
				}

				if (!angular.isDefined(scope.output) && !readOnly) {
					console.log('Multi-Select: Output is undefined.');
					return;
				}

				scope.query = '';
				scope.expanded = {};
				scope.disabledItems = {};
				scope.backExpanded = {};
				scope.selectedItems = {};

				scope.$childProperty = $childProperty;
				scope.$parentProperty = $parentProperty;

				var disabledCounter = [];

				/**
				 * Check if the item has children
				 * @method hasChildren
				 * @param {Object} item (node)
				 * @return LogicalExpression
				 */
				var hasChildren = function (item) {
					return (item[$childProperty] && item[$childProperty].length > 0);
				};

				/**
				 * Check if the item has parents
				 * @method hasParents
				 * @param {Object} item (node)
				 * @return LogicalExpression
				 */
				var hasParents = function (item) {
					return (item[$parentProperty] && item[$parentProperty].length > 0);
				};

				/**
				 * Check if an item is expanded
				 * @method isExpanded
				 * @param {Object} item (node)
				 * @return ConditionalExpression
				 */
				var isExpanded = function (item) {
					return (scope.expanded[item.id] ? true : false);
				};

				/**
				 * Expand or collapse an item and its children
				 * @method expandOrCollapse
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 * @param {Boolean} forceChildren (force expand/collapse on children)
				 */
				var expandOrCollapse = function (item, value, forceChildren) {
					forceChildren = typeof forceChildren !== 'undefined' ? forceChildren : false;

					scope.expanded[item.id] = value;

					if (autoExpandSubChildren || forceChildren) {
						if (hasChildren(item)) {
							// expand all the children
							for (var i in item[$childProperty]) {
								expandOrCollapse(item[$childProperty][i], value, forceChildren);
							}
						}
					}
				};

				/**
				 * Set a value to a item and its children
				 * @method checkItem
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 * @param {Boolean} forceChildren (force check/uncheck on children)
				 */
				var checkItem = function (item, value, forceChildren) {
					// Check optional value
					forceChildren = typeof forceChildren !== 'undefined' ? forceChildren : false;

					// Check allowed if the item is not disabled
					if (!isDisabled(item) && !readOnly) {
						scope.selectedItems[item.id] = {'id': item.id, 'name': item.name, 'selected': value};

						// Disable the entire family
						if (autoDisableFamily) {
							disableFamily(item, value);
						}

						// Select all the children
						else if (autoSelectChildren || forceChildren) {
							checkChildren(item, value);
						}
					}
				};

				/**
				 * Set a value to every children of an item
				 * @method checkChildren
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 */
				var checkChildren = function (item, value) {
					if (hasChildren(item)) {
						for (var i in item[$childProperty]) {
							checkItem(item[$childProperty][i], value, true);
						}
					}
				};

				/**
				 * Check if an item is selected
				 * @method isSelected
				 * @param {Object} item (node)
				 * @return LogicalExpression
				 */
				var isSelected = function (item) {
					if (!readOnly && scope.selectedItems[item.id] && scope.selectedItems[item.id].selected) {
						return scope.selectedItems[item.id].selected;
					}

					return false;
				};

				/**
				 * Check if an item has selected children
				 * @method hasCheckedChildren
				 * @param {Object} item (node)
				 * @return CallExpression
				 */
				var hasCheckedChildren = function (item) {
					// Children
					if (hasChildren(item)) {
						for (var i in item[$childProperty]) {
							if (hasCheckedChildren(item[$childProperty][i])) {
								return true;
							}
						}
					}

					// Leaf
					return isSelected(item);
				};

				/**
				 * Disable an item and its children
				 * @method disableItem
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 */
				var disableItem = function (item, value) {
					var key = item.id ? item.id : item;
					scope.disabledItems[key] = value;

					if (hasChildren(item)) {
						disableChildren(item, value);
					}
				};

				/**
				 * Disable an item's children
				 * @method disableChildren
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 */
				var disableChildren = function (item, value) {
					if (hasChildren(item)) {
						for (var i in item[$childProperty]) {
							disableItem(item[$childProperty][i], value);
						}
					}
				};

				/**
				 * Disable an item's parents
				 * @method disableParent
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 */
				var disableParent = function (item, value) {
					if (hasParents(item)) {
						for (var i = 0; i < item[$parentProperty].length; i++) {
							var parentId = item[$parentProperty][i];

							// Init disabledCounter
							if (disabledCounter[parentId] === undefined || disabledCounter[parentId] < 0) {
								disabledCounter[parentId] = 0;
							}

							if (value === true) {
								disabledCounter[parentId] = disabledCounter[parentId] + 1;
							} else {
								disabledCounter[parentId] = disabledCounter[parentId] - 1;
							}

							var newValue = (disabledCounter[parentId] > 0);
							disableItem(parentId, newValue);
						}
					}
				};

				/**
				 * Disable an item's family (parents & children)
				 * @method disableFamily
				 * @param {Object} item (node)
				 * @param {Boolean} value
				 */
				var disableFamily = function (item, value) {
					disableParent(item, value);
					disableChildren(item, value);
				};

				/**
				 * Check if an item is disabled
				 * @method isDisabled
				 * @param {Object} item (node)
				 * @return Boolean
				 */
				var isDisabled = function (item) {
					if (!isSelected(item)) {
						if (scope.disabledItems[item.id]) {
							return scope.disabledItems[item.id];
						}
					}

					return false;
				};

				/**
				 * Description
				 * @method hasDisabledChild
				 * @param {Object} item (node)
				 * @return Boolean
				 */
				var hasDisabledChild = function (item) {
					if (hasChildren(item)) {
						for (var i in item[$childProperty]) {
							if (isDisabled(item[$childProperty][i])) {
								return true;
							}
						}
					}

					return false;
				};

				/**
				 * Description
				 * @method hasDisabledParent
				 * @param {Object} item (node)
				 * @return Boolean
				 */
				var hasDisabledParent = function (item) {
					if (hasParents(item)) {
						for (var i in item[$parentProperty]) {
							if (isDisabled(item[$parentProperty][i])) {
								return true;
							}
						}
					}

					return false;
				};

				/**
				 * Bind the selected items to the output
				 * @method refreshOutput
				 */
				var refreshOutput = function () {
					scope.output = angular.copy(scope.selectedItems);
				};

				/**
				 * ==========================================================
				 * Scope
				 * ==========================================================
				 */
				scope.hasChildren = hasChildren;
				scope.hasCheckedChildren = hasCheckedChildren;
				scope.hasParents = hasParents;
				scope.isSelected = isSelected;
				scope.isExpanded = isExpanded;
				scope.isDisabled = isDisabled;

				/**
				 * Toggle expand/collapse
				 * @method toggle
				 * @param {Object} item (node)
				 */
				scope.toggle = function (item) {
					if (scope.query.length === 0) {
						scope.expanded[item.id] = !scope.expanded[item.id];
						scope.backExpanded = angular.copy(scope.expanded);
					}
				};

				/**
				 * Expand an item
				 * @method expand
				 * @param {Object} item (node)
				 */
				scope.expand = function (item) {
					expandOrCollapse(item, true);
				};

				/**
				 * Expand every items
				 * @method expandAll
				 */
				scope.expandAll = function () {
					for (var i in scope.input) {
						expandOrCollapse(scope.input[i], true, true);
					}
				};

				/**
				 * Collapse an item
				 * @method collapse
				 * @param {Object} item (node)
				 */
				scope.collapse = function (item) {
					expandOrCollapse(item, false);
				};

				/**
				 * Collapse every items
				 * @method collapseAll
				 */
				scope.collapseAll = function () {
					for (var i in scope.input) {
						expandOrCollapse(scope.input[i], false, true);
					}
				};

				/**
				 * Description
				 * @method revertExpand
				 */
				scope.revertExpand = function () {
					scope.expanded = angular.copy(scope.backExpanded);
				};

				/**
				 * Description
				 * @method search
				 */
				scope.search = function () {
					if (scope.query.length > 0) {
						scope.expandAll();
					} else {
						scope.revertExpand();
					}
				};

				/**
				 * Description
				 * @method resetSearch
				 */
				scope.resetSearch = function () {
					if (scope.query.length > 0) {
						scope.query = '';
						scope.revertExpand();
					}
				};

				/**
				 * Toggle a item selection
				 * @method selectItem
				 * @param {Object} item (node)
				 */
				scope.selectItem = function (item) {
					var check = !isSelected(item);
					checkItem(item, check);

					if (readOnly) {
						scope.toggle(item);
					} else if (autoExpandChildren) {
						scope.expand(item);
					}

					refreshOutput();
				};

				/**
				 * Expand/collapse icon
				 * @method stateCSS
				 * @param {Object} item (node)
				 * @return ObjectExpression
				 */
				scope.stateCSS = function (item) {
					return {
						'expanded': isExpanded(item),
						'collapsed': !isExpanded(item),
						'no-children': !hasChildren(item),
						'hide': scope.query.length > 0
					};
				};

				/**
				 * Description
				 * @method stateItem
				 * @param {Object} item (node)
				 * @return ObjectExpression
				 */
				scope.stateItem = function (item) {
					return {
						'disabled': (hasDisabledChild(item) || isDisabled(item) || hasDisabledParent(item)) && !isSelected(item)
					};
				};

				/**
				 * Check/unchecked/indeterminate icon (using font awesome)
				 * @method stateChecked
				 * @param {Object} item (node)
				 * @return ObjectExpression
				 */
				scope.stateChecked = function (item) {
					return {
						'fa-square-o': !isSelected(item) && !hasCheckedChildren(item),
						'fa-check-square-o': isSelected(item),
						'fa-minus-square-o': !isSelected(item) && hasCheckedChildren(item),
						'hide': readOnly
					};
				};

				/**
				 * Description
				 * @method selectHelper
				 * @param {String} action
				 */
				scope.selectHelper = function (action) {
					switch (action) {
						case 'all':
							for (var i in scope.input) {
								checkChildren(scope.input[i], true);
							}
							refreshOutput();
							scope.expandAll();
							break;

						case 'none':
							for (var j in scope.input) {
								checkChildren(scope.input[j], false);
							}
							refreshOutput();
							break;

						case 'reset':
							console.log('TODO: SelectHelper "reset"');
							break;

						default:
							break;
					}
				};

				// Check the input
				scope.$watch('input', function (newValue, oldValue) {
					if (newValue !== oldValue) {
						// Reset
						scope.selectedItems = {};
						scope.disabledItems = {};
						refreshOutput();
					}

					// Expand the first parents
					angular.forEach(scope.input, function (item) {
						if (!isExpanded(item)) {
							scope.toggle(item);
						}
					});
				}, true);

				// Check the output
				scope.$watch('output', function (newValue) {
					scope.selectedItems = angular.copy(newValue);

					if (autoDisableFamily) {
						angular.forEach(scope.output, function (item) {
							disableFamily(item, true);
						});
					}
				}, true);

			}
		};
	});
})();