(function () {
	'use strict';

	// TODO: Role based authentication
	// See: http://arthur.gonigberg.com/2013/06/29/angularjs-role-based-auth/
	angular.module('angularApp.authentication', ['appConfig', 'appConfig'])

	/**
	 * Routes
	 */
	.config(['$stateProvider', function ($stateProvider) {
		// Login
		$stateProvider
			.state('home', {
				url: '/home',
				templateUrl: 'login.html',
				controller: 'HomeCtrl'
			});
	}])

	/**
	 * LoginService
	 * Login, sets the http authent, logout and checks if the user is logged in
	 */
	.factory('LoginService',
		['$http', 'ipCookie', 'authService',
		function ($http, ipCookie, authService) {

			function encodeBase64 (input) {
				var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
				var output = '';
				var chr1, chr2, chr3 = '';
				var enc1, enc2, enc3, enc4 = '';
				var i = 0;

				do {
					chr1 = input.charCodeAt(i++);
					chr2 = input.charCodeAt(i++);
					chr3 = input.charCodeAt(i++);

					enc1 = chr1 >> 2;
					enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
					enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
					enc4 = chr3 & 63;

					if (isNaN(chr2)) {
						enc3 = enc4 = 64;
					} else if (isNaN(chr3)) {
						enc4 = 64;
					}

					output = output +
						keyStr.charAt(enc1) +
						keyStr.charAt(enc2) +
						keyStr.charAt(enc3) +
						keyStr.charAt(enc4);
					chr1 = chr2 = chr3 = '';
					enc1 = enc2 = enc3 = enc4 = '';
				} while (i < input.length);

				return output;
			}

			var authdata = ipCookie('authdata');
			var loginConfirmed = ipCookie('loginConfirmed') ? true : false;

			if (authdata) {
				$http.defaults.headers.common.Authorization = 'Basic ' + ipCookie('authdata');
			}

			return {
				isLoggedIn: function() {
					return loginConfirmed && (ipCookie('authdata') !== undefined);
				},
				setCredentials: function (username, password) {
					var encoded = encodeBase64(username + ':' + password);
					ipCookie('authdata', encoded, {expires: 30, expirationUnit: 'minutes'});

					$http.defaults.headers.common.Authorization = 'Basic ' + encoded;
					authService.loginConfirmed();
				},
				confirmUser: function () {
					// Remove data just in case
					ipCookie.remove('loginConfirmed');

					loginConfirmed = true;

					// Store the values in cookies
					ipCookie('loginConfirmed', loginConfirmed);
				},
				removeCredentials: function() {
					document.execCommand('ClearAuthenticationCache');

					ipCookie.remove('authdata');
					ipCookie.remove('loginConfirmed');

					loginConfirmed = false;

					delete $http.defaults.headers.common.Authorization;
				}
			};
		}
	])

	/**
	 * Home controller
	 * Redirects to 'app.summary' if the user is logged in
	 * Displays the login form if the user is not logged in
	 */
	.controller('HomeCtrl',
		['$rootScope', '$scope', '$state', '$http', 'LoginService', 'AlertService', 'CATALOGS_RESOURCE_URL',
		function ($rootScope, $scope, $state, $http, LoginService, AlertService, CATALOGS_RESOURCE_URL) {

			$scope.user = { name: '', password: '' };
			var original = angular.copy($scope.user);

			if (LoginService.isLoggedIn()) {
				// Redirect to application
				$state.go('app.summary');
			} else {
				// Remove credentials just in case
				LoginService.removeCredentials();
			}

			// Form submit method
			$scope.submit = function() {
				var user = $scope.user;

				AlertService.clearAll();
				LoginService.setCredentials(user.name, user.password);

				// Reset the form
				$scope.user = angular.copy(original);

				// Test the credentials
				// If the username or password are incorrect,
				// The http-auth-interceptor will broadcast the "login-required" event and redirect to home
				$http.get(CATALOGS_RESOURCE_URL).success(function () {
					LoginService.confirmUser();
					$state.go('app.summary');
				}).error(function () {
					// Error 500 (Internal Server Error)
					AlertService.add('warning', 'Erreur lors de la connexion : Le serveur ne répond pas...');
				});
			};
		}
	])

	/**
	 * Auth controller
	 * Used by the authLink directive
	 */
	.controller('AuthCtrl',
		['$scope', '$state', '$stateParams', 'LoginService', 'AlertService', 'APP_VERSION',
		function ($scope, $state, $stateParams, LoginService, AlertService, APP_VERSION) {
			$scope.isLoggedIn = function() {
				return LoginService.isLoggedIn();
			};

			$scope.logout = function() {
				LoginService.removeCredentials();
				AlertService.add('info', 'Vous êtes maintenant déconnecté.');
				$state.go('home');
			};

			$scope.version = APP_VERSION;
		}
	])

	/**
	 * authLink directive
	 * Displays the logout button
	 */
	.directive('authLink', function() {
		return {
			templateUrl: 'modules/authentication/auth-link.tpl.html',
			restrict: 'C',
			replace: true,
			controller: 'AuthCtrl'
		};
	});
})();