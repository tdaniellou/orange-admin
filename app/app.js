(function () {
	'use strict';

	/**
	 * Constants
	 */
	angular.module('appConfig', [])
		.constant('APPLICATIONS_RESOURCE_URL', '/vode-admin/v1/applications/:id')
		.constant('CATALOGS_RESOURCE_URL', '/vode-admin/v1/catalogs/:id')
		.constant('CATEGORIES_RESOURCE_URL', '/json/:id.json')
		//.constant('CATEGORIES_RESOURCE_URL', '/vode-admin/v1/categories?catalogId=:id')
		.constant('CATEGORIES_RELOAD_URL', '/vode-admin/v1/categories?catalogId=')
		.constant('UNIVERSES_RESOURCE_URL', '/vode-admin/v1/universes/:id?applicationId=:applicationId')
		.constant('PUBLICATIONS_URLS', [
			{name: 'Catalogues', url:'/vode-admin/v1/catalogs/publish'},
			{name: 'Applications', url: '/vode-admin/v1/applications/publish'},
			{name: 'Univers', url: '/vode-admin/v1/universes/publish'}
		])
		.constant('ROLLBACKS_URLS', [
			{name: 'Catalogues', url:'/vode-admin/v1/catalogs/rollback'},
			{name: 'Applications', url: '/vode-admin/v1/applications/rollback'},
			{name: 'Univers', url: '/vode-admin/v1/universes/rollback'}
		])
		.constant('APP_VERSION', 'PROD - NewTV - VOD - G3R4');

	/**
	 * AngularJS Application
	 */
	var app = angular.module('angularApp', [
		'ipCookie',
		'ngAnimate',
		'ngSanitize',
		'ui.router',
		'ui.bootstrap',
		'ui.select2',
		'angularApp.alert',
		'angularApp.authentication',
		'angularApp.application',
		'angularApp.catalog',
		'angularApp.category',
		'angularApp.utils',
		'angularApp.medias',
		'angularApp.menu',
		'angularApp.publication',
		'angularApp.summary',
		'angularApp.universe',
		'http-auth-interceptor',
		'multiSelect',
		'ng.shims.placeholder',
		'angular-loading-bar'
	]);

	app.config(['$stateProvider', '$urlRouterProvider', '$tooltipProvider', '$httpProvider',
		function ($stateProvider, $urlRouterProvider, $tooltipProvider, $httpProvider) {
			// User abstract route
			$stateProvider
				.state('app', {
					abstract: true,
					templateUrl: 'app-main.tpl.html'
				});

			// Redirect unmatched url
			$urlRouterProvider.otherwise('/home');

			// Tooltip config
			$tooltipProvider.options({
				placement: 'left',
				animation: true,
				popupDelay: 0,
				appendToBody: true
			});

			//$httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache';
		}
	]);

	app.run(['$rootScope', '$state', '$stateParams', '$window', 'uiSelect2Config', 'LoginService', 'AlertService',
		function ($rootScope, $state, $stateParams, $window, uiSelect2Config, LoginService, AlertService) {
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;
			$rootScope.closeAlert = AlertService.closeAlert;
			uiSelect2Config.placeholder = '-- Sélectionnez dans la liste --';
			uiSelect2Config.allowClear = false;

			// Redirect user to login page if he is not logged in (ex: cookie has expired)
			if (!LoginService.isLoggedIn()) {
				if (!$state.is('home')) {
					$state.go('home');
				}
			}

			$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
				// Prevent the user from browsing if he is not logged in
				if (!LoginService.isLoggedIn()) {
					if (toState.name !== 'home') {
						AlertService.add('warning', 'Veuillez vous identifier.');
						event.preventDefault();
						$state.go('home');
					}
				}
			});

			$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
				// Scroll to top on page change
				$window.scrollTo(0, 0);
			});

			$rootScope.$on('event:auth-loginRequired', function () {
				// Delete the wrong credentials
				LoginService.removeCredentials();

				// Add the error message
				AlertService.clearAll();
				AlertService.add('danger', 'Nom d\'utilisateur et/ou mot de passe invalide(s).');

				// Redirect to login
				$state.go('home');
			});
		}
	]);

})();