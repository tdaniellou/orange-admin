'use strict';

exports.config = {

	allScriptsTimeout: 11000,

	specs: [
	'test/e2e/*.e2e.js'
	],

	capabilities: { 'browserName': 'firefox' },

	//baseUrl: 'http://10.194.63.27/admintool-spa',

	rootElement: 'body',
	framework: 'jasmine',

	params: {
		login: {
			user: 'a',
			password: 'a'
		},
		url: 'http://localhost:8000',
		catalog: {
			validId: 'e2eCatalogId',
			validType: 'PASS'
		},
		application: {
			validId: 'e2eAppId',
			validQuality: 'HD'
		},
		universe: {
			validId: 'e2eUnivId',
			validVodApp: 'TAB',
			validName: 'e2e universe name'
		}
	},

	jasmineNodeOpts: {
		isVerbose: true,
		showColors: true,
		includeStackTrace: true,
		defaultTimeoutInterval: 30000
	},

	onPrepare: function() {
		// At this point, global 'protractor' object will be set up, and
		// jasmine will be available.
		require('jasmine-reporters');
		jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter('test', true, true, 'jasmine-'));
	}
};
